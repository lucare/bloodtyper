#!/usr/bin/perl
#!C:/Perl/bin/perl.exe

#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################

use Fcntl qw(:DEFAULT :flock);
use strict;
use DBI;
use Getopt::Long;

use FindBin;
use lib $FindBin::Bin.'/my_perl_modules';
use My::wgs_pred_common;

my $ref_fasta_file = "";
my $bed_file_in = "";
my $bam_file_in = "";
my $pileup_file = "";
my $raw_reads_file_out = "";

my $cmdline_result = GetOptions 	(
							"ref_fasta_file=s" => \$ref_fasta_file,
							"bed_file_in=s" => \$bed_file_in,
							"bam_file_in=s" => \$bam_file_in,
							"pileup_file=s" => \$pileup_file,
							"raw_reads_file_out=s" => \$raw_reads_file_out,
						);

my $cmd = "samtools mpileup -BQ0 -d100000 -f $ref_fasta_file -l $bed_file_in $bam_file_in > $pileup_file";
print $cmd."\n";
system($cmd);

my $cmd = "python ".$FindBin::Bin."/supporting_files/mpileup_to_bases_calls.py $pileup_file > $raw_reads_file_out";
print $cmd."\n";
system($cmd);

# delete the pileup file since it is large and we do not need it after this step.
#unlink($pileup_file);