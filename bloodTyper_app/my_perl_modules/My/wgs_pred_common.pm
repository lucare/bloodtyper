#============================= wgs_pred_common.pm ===========================#
#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################

package My::wgs_pred_common;
use strict;
use Cwd;
use FindBin;
use Data::Dumper;
#use warnings;

require Exporter;
our $VERSION = 0.01;              # Or higher
our @ISA = qw(Exporter);
our @EXPORT      = qw   (
                            &get_exported_prediction_data
                            &get_accession_to_use
                            &get_api_key
                            &make_dir
                            &round
							%exported_prediction_data
							%exported_prediction_data_via_antigen_name
							@exported_prediction_system_and_antigen_num_list
							%antigen_info_via_system_type_system_num_antigen_num
							@types_of_antigen_systems
							%system_type_via_short
							%system_short_via_long
							%system_file_short_via_long
							%gene_info
							@gene_list
							%gene_via_genomic_pos
							$app_server
							$app_script
							$app_client_code_dir
							$website_version_override
							$website_root_html_dir
							$api_key
							$send_sample_name_to_typing_service
							$demo
							$sample_name
							$access_code
							$antigen_systems
							$all_samples_summary_stats_dir
							$accession_file
							$accession
							$accession2
							$genome_num
							$sequence_source
							$sequence_type
							$instrument_type
							$probes_to_skip_filename
							$probes_to_force_call_filename
							%accession2_via_accession
							%genome_num_via_accession
							$pipeline_supporting_files_dir
							$root_dir_data
							$root_dir_results
							$signed_out_dir
							$genes_to_predict
                        );       # Symbols to autoexport (:DEFAULT tag)
our @EXPORT_OK   = qw();       # Symbols to export on request
our %EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

# exported package globals go here
use vars qw (
							%exported_prediction_data
							%exported_prediction_data_via_antigen_name
							@exported_prediction_system_and_antigen_num_list
							%antigen_info_via_system_type_system_num_antigen_num
							@types_of_antigen_systems
							%system_type_via_short
							%system_short_via_long
							%system_file_short_via_long
							%gene_info
							@gene_list
							%gene_via_genomic_pos
							$app_server
							$app_script
							$app_client_code_dir
							$website_version_override
							$website_root_html_dir
							$api_key
							$send_sample_name_to_typing_service
							$demo
							$sample_name
							$access_code
							$antigen_systems
							$accession_file
							$accession
							$accession2
							$genome_num
							$sequence_source
							$sequence_type
							$instrument_type
							$probes_to_skip_filename
							$probes_to_force_call_filename
							%accession2_via_accession
							%genome_num_via_accession
							$root_dir_data
							$root_dir_results
							$all_samples_summary_stats_dir
							$pipeline_supporting_files_dir
							$signed_out_dir
							$genes_to_predict
            );

# non-exported package globals go here
#our ();

# initialize package globals ... exported
%exported_prediction_data = ();
%exported_prediction_data_via_antigen_name = ();
@exported_prediction_system_and_antigen_num_list = ();
%antigen_info_via_system_type_system_num_antigen_num = ();
$genes_to_predict = "";

@types_of_antigen_systems = ("Blood Group System", "HPA Gene");
%system_type_via_short = (
	'RBC' => 'Blood Group System',
	'PLT' => 'HPA Gene',
);
%system_short_via_long = (
	'Blood Group System' => 'RBC',
	'HPA Gene' => 'HPA',
);
%system_file_short_via_long = (
	'Blood Group System' => 'RBC',
	'HPA Gene' => 'Platelet',
);


# Read in config file ...
my $antigen_typing_app_dir = "";
my $config_filename = "app_config.txt";
open(IN, $config_filename);
while (<IN>) {
	my $line = $_;
	chomp($line);
	my @fields = split(/\t/, $line);
	if ($fields[0] eq "app_server") {
		$app_server = $fields[1];
	} elsif ($fields[0] eq "app_script") {
		$app_script = $fields[1];
	} elsif ($fields[0] eq "app_client_code_dir") {
		$app_client_code_dir = $fields[1];
	}
}
close(IN);


# Read in api key file ...
sub get_api_key() {
	if ($demo eq "True") {
		$api_key = "demo";	# If there is not api key given then send the sample name to check for allowed samples without apikey
		$sample_name = $accession;
	} else {
		if ($send_sample_name_to_typing_service eq "yes") {
			$sample_name = $accession;
		} else {
			$sample_name = "";
		}
		my $config_filename = "app_key.txt";
		open(IN, $config_filename);
		while (<IN>) {
			my $line = $_;
			chomp($line);
			my @fields = split(/\t/, $line);
			if ($fields[0] eq "app_key") {
				$api_key = $fields[1];
			}
		}
		close(IN);	
	}
}


# determine the root dir for the data ...
my $script_dir = getcwd;
my $a_fpl_dir_prefix = "";
if ($script_dir =~ /^\/Users\/bill\/Dropbox\/BloodCellAntigens\/bloodcellantigens.com\/cgi-bin/) {
	$root_dir_data = "/Users/bill/Patient Data/MedSeq/Molecular/";
	$root_dir_results = "/Users/bill/Patient Data/MedSeq/Molecular/";
}
#print "Using root dir: ".$root_dir."\n";

$pipeline_supporting_files_dir = $FindBin::Bin."/supporting_files/";

#$all_samples_summary_stats_dir = $root_dir."All_Samples_Summary_Stats/";
#&make_dir($all_samples_summary_stats_dir ,"if_does_not_exist");
#print "Using all_samples_summary_stats_dir: ".$all_samples_summary_stats_dir."\n";

$accession_file = $root_dir_data."accession_dictionary.txt";


#$root_dir = $root_dir."02_03_Protein_Validation_Papers"."/";

$accession = "";
$accession2 = "";
$genome_num = "";
$sequence_source = "";
$sequence_type = "";
$instrument_type = "";
%accession2_via_accession = ();
%genome_num_via_accession = ();


sub get_accession_to_use () {
	my ($argv_accession, $argv_root_data_dir, $argv_root_results_dir) = @_;

	if ($argv_root_data_dir) {
		$root_dir_data = $argv_root_data_dir;
		print "Overriding to new root_dir_data: ".$root_dir_data."\n";
	}
	if ($argv_root_results_dir) {
		$root_dir_results = $argv_root_results_dir;
		print "Overriding to new root_dir_results: ".$root_dir_results."\n";
	}

	open (IN, $accession_file);
	while (<IN>) {
		my $line = $_;
		chomp($line);
		#if (!($line =~ /^#/)) {
		#	($accession, $accession2, $genome_num) = split(",", $line);
		#}
		$line =~ s/#//gi;
		my @fields = split(",", $line);
		$accession2_via_accession{$fields[0]} = $fields[1];
		$genome_num_via_accession{$fields[0]} = $fields[2];
	}
	close(IN);
	# override with command line arg ...
	if ($argv_accession) {
		$accession = $argv_accession;
		$accession2 = $accession2_via_accession{$accession};
		$genome_num = $genome_num_via_accession{$accession};
	}
	if ($accession eq "") {
		print "Could not find dictionary info for accession: ".$accession."\n";
		exit();
	}
	print "Working on: "."Genome #".$genome_num." -- ".$accession." -- ".$accession2."\n";
	
	#$antigen_predictions_dir = $working_dir."antigen_typing/";
	
	$signed_out_dir = $root_dir_results."signed_out/";
	
	$website_root_html_dir = "";
	if ($website_version_override ne "") {
		$app_script = 'cgi-bin/a_v'.$website_version_override.'/a.fpl';
		$website_root_html_dir = "a_v".$website_version_override."/";
	}
}


# Read in the gene table ...
%gene_info = ();
@gene_list = ();
%gene_via_genomic_pos = ();
my $antigen_systems_table = $app_client_code_dir."supporting_files/antigen_systems_table.txt";
open(IN, $antigen_systems_table);
my $line = <IN>;
chomp($line);
my @headers = split(/\t/, $line);
my $system_gene_name_for = "";
for my $i (0 .. $#headers) {
	if ($headers[$i] eq "system_gene_name") {
		$system_gene_name_for = $i;
	}
}
while (<IN>) {
	$line = $_;
	chomp($line);
	my @fields = split(/\t/, $line);
	my $gene = $fields[$system_gene_name_for];
	push(@gene_list, $gene);
	for my $i (0 .. $#headers) {
		$gene_info{$gene}{$headers[$i]} = $fields[$i];
		#print "gene_info{$gene}{$headers[$i]}: ".$gene_info{$gene}{$headers[$i]}."\n";
	}
	$gene_info{$gene}{'system_chr'} =~ s/chr//;
	$gene_info{$gene}{'system_symbol'} =~ s/\<sub\>\&alpha\;\<\/sub\>/alpha/g;
	$gene_info{$gene}{'system_symbol'} =~ s/\<sub\>\&beta\;\<\/sub\>/beta/g;
	
	# Set variable so we can look up gene name via position
	#print "gene_info{$gene}{'system_chr'}: ".$gene_info{$gene}{'system_chr'}."\n";
	#print "gene_info{$gene}{'system_gene_start_by_genomic_pos'}: ".$gene_info{$gene}{'system_gene_start_by_genomic_pos'}."\n";
	#print "gene_info{$gene}{'system_gene_end_by_genomic_pos'}: ".$gene_info{$gene}{'system_gene_end_by_genomic_pos'}."\n";
	if ($gene_info{$gene}{'system_chr'} ne "" && $gene_info{$gene}{'system_gene_start_by_genomic_pos'} ne "" && $gene_info{$gene}{'system_gene_end_by_genomic_pos'} ne "") {
		if ($gene_info{$gene}{'system_gene_start_by_genomic_pos'} < $gene_info{$gene}{'system_gene_end_by_genomic_pos'}) {
			$gene_info{$gene}{'gene_direction'} = "f";
			$gene_info{$gene}{'system_gene_first_genomic_pos'} = $gene_info{$gene}{'system_gene_start_by_genomic_pos'};
			$gene_info{$gene}{'system_gene_last_genomic_pos'} = $gene_info{$gene}{'system_gene_end_by_genomic_pos'};
		} else {
			$gene_info{$gene}{'gene_direction'} = "r";
			$gene_info{$gene}{'system_gene_first_genomic_pos'} = $gene_info{$gene}{'system_gene_end_by_genomic_pos'};
			$gene_info{$gene}{'system_gene_last_genomic_pos'} = $gene_info{$gene}{'system_gene_start_by_genomic_pos'};
		}
	}
	
	#print $gene."\n";
	#print "gene_info{$gene}{'system_gene_first_genomic_pos'}: ".$gene_info{$gene}{'system_gene_first_genomic_pos'}."\n";
	for my $pos ($gene_info{$gene}{'system_gene_first_genomic_pos'} .. $gene_info{$gene}{'system_gene_last_genomic_pos'}) {
		$gene_via_genomic_pos{$gene_info{$gene}{'system_chr'}.":".$pos} = $gene;
		#print $gene_via_genomic_pos{$gene_info{$gene}{'system_chr'}.":".$pos}."\n";
	}
}
close(IN);
#print Dumper(@gene_list)."\n";
#print Dumper(\%gene_info)."\n";
#print Dumper(\%gene_via_genomic_pos)."\n";

##### MISC Functions ...
sub round {
	my ($number,$decimals) = @_;

    return sprintf("%.".$decimals."f", $number)
}

sub make_dir () {
    my ($dir_to_make, $make_condition) = @_;
    
    if ($make_condition eq "if_does_not_exist") {
        if (!(-e $dir_to_make)) {
        	mkdir ($dir_to_make);
        }
    }
}

1;
