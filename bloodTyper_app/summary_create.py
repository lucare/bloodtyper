#========================== summary_create.py ==========================#
#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################


import sys
import os
import argparse
import math
import pipes
import subprocess
import json
import re
import os.path
import glob
import string
import pprint
from collections import defaultdict
from nested_dict import nested_dict

class _defaultdict(defaultdict):
    def __add__(self, other):
        return other

def tree():
    return _defaultdict(tree)

summary_index_html_template = ''
with open('bloodTyper_app/summary_create_template.html', 'r') as myfile:
    summary_index_html_template=myfile.read()

summary_index_html_item_template = """
	<span class="list-group-item">
	    <h4><NAME></h4>
	    <ABO>
	    <p><b>Full Results:</b> <FULL_RESULTS_HTML> | <FULL_RESULTS_TXT></p>
	    <p><OTHER_LINKS></p>
	</span>
"""

# For sorting ...
letters = tuple(string.ascii_letters)
all=string.maketrans('','')
nodigs=all.translate(all, string.digits)
def numlettersort(x):
	if x.startswith(letters):
		return float(999)
	else:
		return float(x.translate(all, nodigs))
def changesort(x):
	#print(x)
	#m = re.search(r".*g\.(.*?)(,|\)| )", x)
	m = re.search(r".*g\.(.*?)(_.*)?(,|\)| )", x)
	if m:
		#print m.group(1)
		number = m.group(1)
		return float(number)
	else:
		return float(999)
	#if x.startswith(letters):
	#	return float(999)
	#else:
	#	return float(x.translate(all, nodigs))

#example:  python summary_create.py

# main commandline parser
parser = argparse.ArgumentParser(description='summary_create',
epilog="""""",fromfile_prefix_chars='@')

parser.add_argument('-d','--data_dir',dest='data_dir', action='store', default='', help='data_dir')
parser.add_argument('-t','--typing_results_dir_root',dest='typing_results_dir_root', action='store', default='', help='typing_results_dir_root')			   
parser.add_argument('-p','--project',dest='project', action='store', default='', help='project')
parser.add_argument('-b','--batch',dest='batch', action='store', default='', help='batch')
parser.add_argument('-a','--accession',dest='accession', action='store', default='', help='accession')
parser.add_argument('-s','--skip',dest='skip', action='store',default='', help='skip this step computation')
parser.add_argument('-as','--antigen_systems',dest='antigen_systems', action='store',default='', help='antigen_systems')
parser.add_argument('-dbsnp','--dbsnp_file',dest='dbsnp_file', action='store',default='', help='dbsnp_file')



args = parser.parse_args()
accession=args.accession
data_dir=args.data_dir
batch=args.batch
antigen_systems=args.antigen_systems.strip().split(',')
dbsnp_file=args.dbsnp_file
print 'antigen_systems: ' + str(antigen_systems)
typing_results_dir_root=args.typing_results_dir_root
print 'Skipping: ' + args.skip

print 'cwd: '+os.getcwd()

accessions_to_process_list  = [
]

ref_fasta_file = '../../reference_seqs/Homo_sapiens_assembly19-samtoolsfixed.fasta'
seqs_dir = data_dir+'raw_data/seqs/'
alignments_dir = data_dir+'raw_data/alignments/'
if (typing_results_dir_root == '' and data_dir != ''):
	typing_results_dir_root = data_dir+'typing_results/'
print 'dbsnp_file: '+dbsnp_file
print 'data_dir: '+data_dir
print 'seqs_dir: '+seqs_dir
print 'alignments_dir: '+alignments_dir
print 'typing_results_dir_root: '+typing_results_dir_root

# create some dirs that are needed ...
results_summary_dir_subroot = typing_results_dir_root+'_RESULTS_SUMMARY/'
if not os.path.exists(results_summary_dir_subroot):
	print ('Creating results_summary_dir_subroot: '+results_summary_dir_subroot)
	os.makedirs(results_summary_dir_subroot)


# Get list of accessions to process ...
if args.accession != '':
	accessions_to_process_list.append(args.accession)
else:
	for file in glob.glob(typing_results_dir_root+'*'):
		if os.path.isdir(file):
			dir = file.replace(typing_results_dir_root, '')
			if not dir.startswith('_'):
				accessions_to_process_list.append(file.replace(typing_results_dir_root, ''))

summary_html_via_antigen_system = {}
for antigen_system in antigen_systems:
	# create some dirs that are needed ...
	results_summary_dir = results_summary_dir_subroot+antigen_system+'/'
	if not os.path.exists(results_summary_dir):
		print ('Creating results_summary_dir: '+results_summary_dir)
		os.makedirs(results_summary_dir)

	# Read in exported results ...
	ISBT_system_nums = []
	ISBT_antigen_nums_via_system_num = defaultdict(list)
	system_name_via_system_num = defaultdict(dict)
	antigen_name_via_via_system_and_antigen_num = defaultdict(dict)
	typing_result_via_accession_system_and_antigen_num = defaultdict(dict)
	typing_result_via_accession_and_antigen_name = defaultdict(dict)
	for accession in sorted(accessions_to_process_list):
		antigen_export_result_file = typing_results_dir_root+accession+'/'+antigen_system+'_antigens/'+accession+'_Combined_'+antigen_system+'_antigen_export.txt'
		if os.path.isfile(antigen_export_result_file):
			print "Reading: "+antigen_export_result_file
			file_in = open(antigen_export_result_file,'r')
			typing_results = {}
			with open(antigen_export_result_file) as f:
			    line = f.readline()
			    headers = line.strip().split('\t')
			    for line in f:
			    	#system_num	system_name	antigen_num	antigen_name	antigen_status	gene_allele_nt_coverage
			    	#001	ABO	1	A	-	38:33:25:32:19:36:26:26:4
					line = line.strip()
					fields = line.strip().split('\t')
					field_via_header = {}
					for i, value in enumerate(fields):
						field_via_header[headers[i]] = value
						#print('field_via_header: '+headers[i]+' -- '+field_via_header[headers[i]])
					system_num =  field_via_header['system_num']
					antigen_num = field_via_header['antigen_num']
					antigen_name = field_via_header['antigen_name']
					system_antigen_num = str(system_num)+':'+str(antigen_num)
					if system_num not in ISBT_system_nums:
						ISBT_system_nums.append(system_num)
					if antigen_num not in ISBT_antigen_nums_via_system_num.get(system_num,[]):
						ISBT_antigen_nums_via_system_num[system_num].append(antigen_num)
					system_name_via_system_num[system_antigen_num] = field_via_header['system_name']
					antigen_name_via_via_system_and_antigen_num[system_antigen_num] = field_via_header['antigen_name']
					typing_result_via_accession_system_and_antigen_num[accession][system_antigen_num] = field_via_header.get('antigen_status', '')
					typing_result_via_accession_and_antigen_name[accession][antigen_name] = field_via_header.get('antigen_status', '')
	
	
	# Create the antigen typing summary file ...
	summary_file = results_summary_dir+'_'+antigen_system+'_TYPING_SUMMARY.txt'
	print 'Creating Summary file: '+summary_file
	fileout = open(summary_file, 'w')
	header_line_1 = ['']
	header_line_2 = ['']
	header_line_3 = ['']
	header_line_4 = ['ACCESSION']
	for system_num in sorted(ISBT_system_nums, key=int) :
			for antigen_num in sorted(ISBT_antigen_nums_via_system_num[system_num], key = lambda k: numlettersort(k)):
				system_antigen_num = str(system_num)+':'+str(antigen_num)
				header_line_1.append(system_num)
				header_line_2.append(system_antigen_num)
				header_line_3.append(system_name_via_system_num[system_antigen_num])
				header_line_4.append(antigen_name_via_via_system_and_antigen_num[system_antigen_num])
	fileout.write('\t'.join(header_line_1)+'\n')
	fileout.write('\t'.join(header_line_2)+'\n')
	fileout.write('\t'.join(header_line_3)+'\n')
	fileout.write('\t'.join(header_line_4)+'\n')
	for accession in sorted(accessions_to_process_list):
		result_line = [accession]
		for system_num in sorted(ISBT_system_nums, key=int) :
			for antigen_num in sorted(ISBT_antigen_nums_via_system_num[system_num], key = lambda k: numlettersort(k)):
				system_antigen_num = str(system_num)+':'+str(antigen_num)
				result_line.append(typing_result_via_accession_system_and_antigen_num.get(accession, {}).get(system_antigen_num, ''))
		fileout.write('\t'.join(result_line)+'\n')
	fileout.close()
	
	
	# Read in the copy number files ...
	if antigen_system == 'RBC':
		cnv_line_via_accession = {}
		header_line = ''
		for accession in sorted(accessions_to_process_list):
			cnv_result_file = typing_results_dir_root+accession+'/RBC_antigens/'+accession+'_copy_number_analysis.txt'
			if os.path.isfile(cnv_result_file):
				print "Reading: "+cnv_result_file
				with open(cnv_result_file) as f:
				    line = f.readline()
				    header_line = line
				    line = f.readline()
				    cnv_line_via_accession[accession] = line
		summary_file = results_summary_dir+'CNV_SUMMARY.txt'
		print 'Creating Summary file: '+summary_file
		fileout = open(summary_file, 'w')	    	
		header_line
		fileout.write('accession'+'\t'+header_line+'\n')
		for accession in sorted(accessions_to_process_list):
			fileout.write(accession+'\t'+cnv_line_via_accession.get(accession, '')+'\n') 
		fileout.close()
	
	
	# Read in the structural change files ...
	if antigen_system == 'RBC':
		RHD_zygosity_via_accession = {}
		structural_changes_line_via_accession = {}
		for accession in sorted(accessions_to_process_list):
			result_file = typing_results_dir_root+accession+'/RBC_antigens/'+accession+'_pre_typing_structural_gene_changes_found.txt'
			if os.path.isfile(result_file):
				print "Reading: "+result_file
				result_lines = []
				with open(result_file) as f:
					line = f.readline()
					line_split = line.strip().split('\t')
					RHD_zygosity_via_accession[accession] = line_split[1] if 1 < len(line_split) else ''
					for line in f:
						line = line.strip()
						result_lines.append(line)
				structural_changes_line_via_accession[accession] = ','.join(result_lines)
		summary_file = results_summary_dir+'STRUCTURAL_CHANGES_SUMMARY.txt'
		print 'Creating Summary file: '+summary_file
		fileout = open(summary_file, 'w')	    	
		header_line = 'ACCESSION\tRHD_ZYGOSITY\tSTRUCTURAL_CHANGES'
		fileout.write(header_line+'\n')
		for accession in sorted(accessions_to_process_list):
			fileout.write(accession+'\t'+RHD_zygosity_via_accession.get(accession, '')+'\t'+structural_changes_line_via_accession.get(accession, '')+'\n') 
		fileout.close()
	
	# read in dbSNP file ...
	dbsnp_data_via_chrpos = nested_dict()
	if os.path.isfile(dbsnp_file) and 0:
		print "Reading: "+dbsnp_file
		with open(dbsnp_file) as f:
			for line in f:
				#chr1    25608548        25608549        rs111989070     C/T
				line = line.strip()
				fields = line.strip().split('\t')
				chr = fields[0]
				pos_start = fields[1]
				pos_end = fields[2]
				rsnum = fields[3]
				rschange = fields[4]
				#print 'rsnum: '+rsnum
				dbsnp_data_via_chrpos[chr+':'+pos_end]['rsnum'] = rsnum
				#print 'rsnum: '+dbsnp_data_via_chrpos[chr+':'+pos_end]['rsnum']
				dbsnp_data_via_chrpos[chr+':'+pos_end]['rschange'] = rschange
	
	# Summarize the the nt change files ...
	change_file_postfixes = ['alleles_found', 'other_changes_missense', 'other_changes_not_silent_not_missense', 'other_changes_poorSeq', 'other_changes_silent']
	#change_file_postfixes = ['other_changes_not_silent_not_missense']
	for change_file_postfix in change_file_postfixes:
		changes_summary_dir = results_summary_dir+change_file_postfix+'_SUMMARY'+'/'
		if not os.path.exists(changes_summary_dir):
			print ('Creating changes_summary_dir: '+changes_summary_dir)
			os.makedirs(changes_summary_dir)
		result_via_accession_gene = tree()
		genes = []
		regions = []
		change_list_via_gene_region = nested_dict(1, list)
		accessions_via_gene_region_change = nested_dict(1, list)
		accessions_total_num = 0
		dbsnp_data_via_change = nested_dict()
		for accession in sorted(accessions_to_process_list):
			result_file = typing_results_dir_root+accession+'/RBC_antigens/'+accession+'_'+change_file_postfix+'.txt'	
			if os.path.isfile(result_file):
				accessions_total_num = accessions_total_num + 1
				print "Reading: "+result_file
				with open(result_file) as f:
				    for line in f:
						#GYPE	CDS	c.38(g.25055, chr4:144,801,662)G>A [missense p.Gly13Glu]
						fields = line.strip().split('\t')
						gene = ''
						region = ''
						changes = ''
						if change_file_postfix == 'alleles_found':
							gene = fields[0]
							changes = fields[1] if 1 < len(fields) else ''
						else:
							gene = fields[0]
							region = fields[1] if 1 < len(fields) else ''
							changes = fields[2] if 2 < len(fields) else ''
							if changes:
								changes_split = changes.split(';')
								for change in changes_split:
									accessions_via_gene_region_change[gene+region+change].append(accession)
									if change not in change_list_via_gene_region[gene+region]:
										change_list_via_gene_region[gene+region].append(change)
										if os.path.isfile(dbsnp_file):
											m = re.search(r"(chr\d+):(.*)\)", change)
											if m:
												chr = m.group(1)
												pos = m.group(2)
												pos = pos.replace(',', '')
												rsnum = ''
												if dbsnp_data_via_chrpos[chr+':'+pos]['rsnum']:
													rsnum = dbsnp_data_via_chrpos[chr+':'+pos]['rsnum']
												#print 'rsnum: '+str(rsnum)
												rschange = ''
												if dbsnp_data_via_chrpos[chr+':'+pos]['rschange']:
													rschange = dbsnp_data_via_chrpos[chr+':'+pos]['rschange']
												#print 'rschange: '+str(rschange)
												dbsnp_data_via_change[change]['rsnum'] = rsnum
												dbsnp_data_via_change[change]['rschange'] = rschange
										#		tosearch = chr+'\\t.*\\t'+pos
										#		cmd = 'grep -P "'+tosearch+'" '+dbsnp_file
										#		print 'cmd: '+cmd
										#		proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
										#		(out, err) = proc.communicate()
										#		print "program output:", out
							#print "gene: "+gene
							#print "region: "+region
							#print "changes: "+changes
							#print str(changes_split)
						if gene not in genes:
							genes.append(gene)
						if region not in regions:
							regions.append(region)
						result_via_accession_gene[accession][gene][region] = changes
		for gene in genes:	
			summary_file = changes_summary_dir+gene+'_'+change_file_postfix+'_summary.txt'
			print 'Creating Summary file: '+summary_file
			fileout = open(summary_file, 'w')	    	
			fileout.write('gene'+'\t'+'region'+'\t'+'change'+'\t'+'rsnum'+'\t'+'rschange'+'\t'+'num_samples_with_change'+'\t'+'samples_total_num'+'\n')
			for region in regions:
				#for change in change_list_via_gene_region[gene+region]:
				for change in sorted(change_list_via_gene_region[gene+region], key = lambda k: changesort(k)):
					#print "change: "+change
					num_accessions_with_change = len(accessions_via_gene_region_change[gene+region+change])
					percent_accession_with_change = int(round(num_accessions_with_change / accessions_total_num) * 100)
					fileout.write(gene+'\t'+region+'\t'+change+'\t'+str(dbsnp_data_via_change[change]['rsnum'])+'\t'+str(dbsnp_data_via_change[change]['rschange'])+'\t'+str(num_accessions_with_change)+'\t'+str(accessions_total_num)+'\n') 
					#fileout.write(accession+'\t'+region+'\t'+result_via_accession_gene.get(accession, {}).get(gene, {}).get(region, '')+'\n') 
			fileout.close()	
	
	# If there is a truth file compare ...
	truth_file = typing_results_dir_root+'_CORRECTED_FINAL_TYPINGS-'+antigen_system+'.txt'
	if os.path.isfile(truth_file):
		truth_antigen_name_list = []
		truth_accession_list = []
		truth_typing_result_via_accession_and_antigen_name = defaultdict(dict)
		if os.path.isfile(truth_file):
			print "Reading: "+truth_file
			with open(truth_file) as f:
				line = f.readline()
				headers = line.strip().split('\t')
				for line in f:
				#ACCESSION	ABO RhD	ABO	A	B	M	N	S	s	U	D	c	C	e	E	V	VS	Lu(a)	Lu(b)	K	k	Kp(a)	Kp(b)	Js(a)	Js(b)	Fy(a)	Fy(b)	Jk(a)	Jk(b)	Di(a)	Di(b)	Sc1	Sc2	Do(a)	Do(b)	Hy	Jo(a)	Co(a)	Co(b)	LW(a)	LW(b)
				#MEDSEQ-001	B POSITIVE	B	-	+	+	-	+	+	+	+	-	+	+	-	-	-	-	+	-	+	-	+	-
					line = line.strip()
					fields = line.strip().split('\t')
					accession = fields[0]
					if accession not in truth_accession_list:
						truth_accession_list.append(accession)
					for i, value in enumerate(fields):
						if headers[i] != 'SAMPLE':
							truth_typing_result_via_accession_and_antigen_name[accession][headers[i]] = value
							if headers[i] not in truth_antigen_name_list:
								truth_antigen_name_list.append(headers[i])				
		#pprint.pprint(dict(truth_typing_result_via_accession_and_antigen_name))
		#pprint.pprint(dict(typing_result_via_accession_and_antigen_name))
		#print str(truth_accession_list)
		#print str(truth_antigen_name_list)
		summary_file = results_summary_dir+antigen_system+'_COMPARE_'+antigen_system+'.txt'
		print 'Creating Compare file: '+summary_file
		fileout = open(summary_file, 'w')
		line_out = ["sample"]
		line_out.extend(truth_antigen_name_list)
		fileout.write('\t'.join(line_out)+'\n')
		for accession in truth_accession_list:
			line_out = [accession]
			for antigen_name in truth_antigen_name_list:
				truth = truth_typing_result_via_accession_and_antigen_name.get(accession, {}).get(antigen_name, '')	
				result_to_compare = typing_result_via_accession_and_antigen_name.get(accession, {}).get(antigen_name, '')
				if "+" in result_to_compare:
					result_to_compare = "+"
				if "-" in result_to_compare:
					result_to_compare = "-"
				compare = ''
				if truth != '':
					if result_to_compare == '':
						compare = 'NC'
					else:
						if truth == result_to_compare:
							compare = "C"
						else:
							compare = "D"
					if compare == 'D':
						print accession
						print antigen_name + ': ' + truth + ' vs ' + result_to_compare + ' ==> ' + compare
				line_out.append(compare)
			fileout.write('\t'.join(line_out)+'\n')
		fileout.close()
	
	# create ABO summary file ...
	if antigen_system == 'RBC':
		summary_file = results_summary_dir+'ABO_TYPING_SUMMARY.txt'
		print 'Creating ABO Summary file: '+summary_file
		fileout = open(summary_file, 'w')
		fileout.write('accession'+'\t'+'ABO_type'+'\t'+'coverage_average'+'\t'+'coverage'+'\n')
		for accession in sorted(accessions_to_process_list):
			carbo_result_file = typing_results_dir_root+accession+'/RBC_antigens/'+accession+'_carbo_antigens.txt'
			print("Looking for: "+carbo_result_file)
			if os.path.isfile(carbo_result_file):
				file_in = open(carbo_result_file,'r')
				typing_results = {}
				typing_results['ABO'] = ""
				coverage_via_gene = {}
				coverage_via_gene['ABO'] = ""
				coverage_average = ''
				for line in file_in:
					#print line
					line = line.strip()
					fields = line.strip().split('\t')
					antigen = fields[0]
					typing_result = ''
					coverage = ''
					if len(fields) > 1:
						typing_result = fields[1]
					if len(fields) > 3:
						coverage = fields[3]
					if antigen == 'ABO':
						coverage_average = ''
						if coverage:
							values = coverage.split(':')
							sum = 0
							total = 0;
							for value in values:
								sum = sum + int(value)
								total = total + 1
							coverage_average = str(sum/total)
						typing_results[antigen] = typing_result
						coverage_via_gene[antigen] = coverage
						typing_result_via_accession_and_antigen_name[accession]['ABO'] = typing_result
				file_in.close()
				#if typing_results.get('ABO','') != '' and coverage_via_gene.get('ABO','') != '':
				fileout.write(accession+'\t'+typing_results.get('ABO','')+'\t'+coverage_average+'\t'+coverage_via_gene.get('ABO','')+'\n')
		fileout.close()
	
	
	# Read in coverage per gene file ...
	coverage_avg_via_accession_gene = defaultdict(dict)
	gene_list = []
	for accession in sorted(accessions_to_process_list):
		result_file = typing_results_dir_root+accession+'/RBC_antigens/'+accession+'_gene_coverage_average_summary.txt'
		if os.path.isfile(result_file):
			print "Reading: "+result_file
			with open(result_file) as f:
				line = f.readline()
				genes = line.strip().split('\t')
				for gene in genes:
					if gene not in gene_list:
						gene_list.append(gene)
				line = f.readline()
				line_split = line.strip().split('\t')
				for idx, val in enumerate(genes):
					coverage_avg_via_accession_gene[accession][val] = line_split[idx]
	summary_file = results_summary_dir+'gene_coverage_average_summary.txt'
	print 'Creating Summary file: '+summary_file
	fileout = open(summary_file, 'w')	    	
	fileout.write('ACCESSION\t'+'\t'.join(gene_list)+'\n')
	for accession in sorted(accessions_to_process_list):
		line_array = []
		for gene in gene_list:
			if coverage_avg_via_accession_gene.get(accession, {}).get(gene, {}):
				line_array.append(coverage_avg_via_accession_gene.get(accession, {}).get(gene, {}))
		fileout.write(accession+'\t'+'\t'.join(line_array)+'\n') 
	fileout.close()
	
	# Read in het per gene file ...
	het_percentage_via_accession_gene = defaultdict(dict)
	gene_list = []
	for accession in sorted(accessions_to_process_list):
		result_file = typing_results_dir_root+accession+'/RBC_antigens/'+accession+'_gene_het_percentage_summary.txt'
		if os.path.isfile(result_file):
			print "Reading: "+result_file
			with open(result_file) as f:
				line = f.readline()
				genes = line.strip().split('\t')
				for gene in genes:
					if gene not in gene_list:
						gene_list.append(gene)
				line = f.readline()
				line_split = line.strip().split('\t')
				for idx, val in enumerate(genes):
					het_percentage_via_accession_gene[accession][val] = line_split[idx]
	summary_file = results_summary_dir+'gene_het_percentage_summary.txt'
	print 'Creating Summary file: '+summary_file
	fileout = open(summary_file, 'w')	    	
	fileout.write('ACCESSION\t'+'\t'.join(gene_list)+'\n')
	for accession in sorted(accessions_to_process_list):
		line_array = []
		for gene in gene_list:
			if het_percentage_via_accession_gene.get(accession, {}).get(gene, {}):
				line_array.append(het_percentage_via_accession_gene.get(accession, {}).get(gene, {}))
		fileout.write(accession+'\t'+'\t'.join(line_array)+'\n') 
	fileout.close()
	
	# create summary file ...

	summary_html_via_antigen_system[antigen_system] = ''

	for accession in sorted(accessions_to_process_list):
		antigen_export_result_file_html = accession+'/'+antigen_system+'_antigens/'+accession+'_Combined_'+antigen_system+'_antigens.html'
		antigen_export_result_file_txt = accession+'/'+antigen_system+'_antigens/'+accession+'_Combined_'+antigen_system+'_antigen_export.txt'
		entry = summary_index_html_item_template
		entry = entry.replace('<NAME>', accession)
		entry = entry.replace('<FULL_RESULTS_HTML>', '<a href="'+antigen_export_result_file_html+'">Web Page</a>')
		entry = entry.replace('<FULL_RESULTS_TXT>', '<a href="'+antigen_export_result_file_txt+'">Text File</a>')
		if antigen_system == 'RBC':
			entry = entry.replace('<ABO>', 'ABO: '+typing_result_via_accession_and_antigen_name.get(accession, {}).get('ABO', ''))
		else:
			entry = entry.replace('<ABO>', '')
		other_files = []
		other_link_name = []
		if antigen_system == 'RBC':
			other_files.append(accession+'_copy_number_analysis.txt')
			other_link_name.append("Copy Number Analysis")
		if antigen_system == 'RBC':
			other_files.append(accession+'_pre_typing_structural_gene_changes_found.txt')
			other_link_name.append("Structural Changes")
		other_files.append(accession+'_gene_coverage_average_summary.txt')
		other_link_name.append("Average Gene Sequence Depth of Coverage")
		other_files.append(accession+'_gene_het_percentage_summary.txt')
		other_link_name.append("Gene Heterozygous Position Percentages")
		other_files.append(accession+'_alleles_found.txt')
		other_link_name.append("Alleles Found")
		other_files.append(accession+'_other_changes_silent.txt')
		other_link_name.append("Other Changes - Slient")
		other_files.append(accession+'_other_changes_missense.txt')
		other_link_name.append("Other Changes - Missense")
		other_files.append(accession+'_other_changes_not_silent_not_missense.txt')
		other_link_name.append("Other Changes - Intron and UTR")
		other_files.append(accession+'_other_changes_poorSeq.txt')
		other_link_name.append("Poor/Low Sequence Positions")
		other_links_html = ""
		for idx, link in enumerate(other_files):
			link = accession+'/'+antigen_system+'_antigens/' + link
			name = other_link_name[idx]
			other_links_html = other_links_html + '<a href="'+link+'">'+name+'</a><BR>'
		entry = entry.replace('<OTHER_LINKS>', other_links_html)
		
		summary_html_via_antigen_system[antigen_system] = summary_html_via_antigen_system[antigen_system] + entry

summary_index_html_template = summary_index_html_template.replace('<INSERT_RBC_ITEMS>', summary_html_via_antigen_system.get('RBC', ''))
summary_index_html_template = summary_index_html_template.replace('<INSERT_PLT_ITEMS>', summary_html_via_antigen_system.get('PLT', ''))	
summary_html_file = typing_results_dir_root+'index.html'	
fileout = open(summary_html_file, 'w')
fileout.write(summary_index_html_template)
fileout.close()
	
	
	
