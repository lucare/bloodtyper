#!/usr/bin/perl
#!C:/Perl/bin/perl.exe

#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################

use Fcntl qw(:DEFAULT :flock);
use strict;
#use Data::Dump qw(dump);
#use LWP::UserAgent;
use LWP::UserAgent::Determined;
use HTTP::Request::Common qw(POST);
use File::Copy;
use Getopt::Long;
use Compress::LZF;
use MIME::Base64;
use Digest::MD5 qw(md5_hex);

use FindBin;
use lib $FindBin::Bin.'/my_perl_modules';
use My::wgs_pred_common;

my $accession_cleaned_cmdline = "";
my $root_data_dir_cmd_line = "";
my $root_results_dir_cmd_line = "";
my $nt_base_coverage_cutoff_for_call = "";
my $nt_base_coverage_cutoff_low_coverage_call = "";
my $run_timestamp = "";
my $no_coverage_plot = "";
my $use_experimental_phasing_data = "";
my $include_misphase_in_results = "";
my $show_allele_network_graphs = "";
my $show_coverage_table_regions_list = "";
my $reference_GRCh_genome_build_num = "";
my $cmdline_result = GetOptions 	(
							"accession_cleaned=s" => \$accession_cleaned_cmdline,
							"root_data_dir=s" => \$root_data_dir_cmd_line,
							"root_results_dir=s" => \$root_results_dir_cmd_line,
							"sequence_source=s" => \$sequence_source,
							"instrument_type=s" => \$instrument_type,
							"sequence_type=s" => \$sequence_type,
							"antigen_systems=s" => \$antigen_systems,
							"genes_to_predict=s" => \$genes_to_predict,
							"nt_base_coverage_cutoff_for_call=s" => \$nt_base_coverage_cutoff_for_call,
							"nt_base_coverage_cutoff_low_coverage_call=s" => \
							$nt_base_coverage_cutoff_low_coverage_call,
							"run_timestamp=s" => \$run_timestamp,
							"no_coverage_plot=s" => \$no_coverage_plot,
							"use_experimental_phasing_data=s" => \$use_experimental_phasing_data,
							"include_misphase_in_results=s" => \$include_misphase_in_results,
							"show_allele_network_graphs=s" => \$show_allele_network_graphs,
							"show_coverage_table_regions_list=s" => \$show_coverage_table_regions_list,
							"reference_GRCh_genome_build_num=s" => \$reference_GRCh_genome_build_num,
							"send_sample_name_to_typing_service=s" => \$send_sample_name_to_typing_service,
							"website_version_override=s" => \$website_version_override,
							"demo=s" => \$demo,
							"access_code=s" => \$access_code,
						);
$show_coverage_table_regions_list =~ s/ //gi;
print "Sequence source: ".$sequence_source."\n";
print "Instrument type: ".$instrument_type."\n";
print "Sequence type: ".$sequence_type."\n";
my @antigen_system_list = split(/,/, $antigen_systems);
print "Antigen Systems: ".join(",", @antigen_system_list)."\n";
my @genes_to_predict_list = split(/,/, $genes_to_predict);
print "genes_to_predict: ".join(",", @genes_to_predict_list)."\n";
print "nt_base_coverage_cutoff_for_call: ".$nt_base_coverage_cutoff_for_call."\n";
print "nt_base_coverage_cutoff_low_coverage_call: ".$nt_base_coverage_cutoff_low_coverage_call."\n";
print "no_coverage_plot: ".$no_coverage_plot."\n";
print "run_timestamp: ".$run_timestamp."\n";
print "root_data_dir_cmd_line: ".$root_data_dir_cmd_line."\n";
print "root_results_dir_cmd_line: ".$root_results_dir_cmd_line."\n";
print "use_experimental_phasing_data: ".$use_experimental_phasing_data."\n";
print "include_misphase_in_results: ".$include_misphase_in_results."\n";
print "show_allele_network_graphs: ".$show_allele_network_graphs."\n";
print "show_coverage_table_regions_list: ".$show_coverage_table_regions_list."\n";
print "reference_GRCh_genome_build_num: ".$reference_GRCh_genome_build_num."\n";
print "send_sample_name_to_typing_service: ".$send_sample_name_to_typing_service."\n";
print "website_version_override: ".$website_version_override."\n";
print "website_root_html_dir: ".$website_root_html_dir."\n";
print "demo: ".$demo."\n";
print "access_code: ".$access_code."\n";

&get_accession_to_use($accession_cleaned_cmdline, $root_data_dir_cmd_line, $root_results_dir_cmd_line);
&get_api_key();

my @types_of_antigen_systems = ();
if ($antigen_systems =~ /RBC/) {
	push(@types_of_antigen_systems, "Blood Group System");
}
if ($antigen_systems =~ /PLT/) {
	push(@types_of_antigen_systems, "HPA Gene");
}
if ($antigen_systems =~ /HNA/) {
	push(@types_of_antigen_systems, "HNA Gene");
}
print "types_of_antigen_systems: ".join(",", @types_of_antigen_systems)."\n";


my $gene_seq_dir = "";
if ($accession eq "Historical_Reference" || $accession eq "Human_Reference_Genome_(hg19)") {
	$gene_seq_dir = "../a/blood_group_system_files/";
} else {
	$gene_seq_dir = $root_dir_data.$accession."/gene_seq/";
}
my $gene_base_stats_dir = $root_dir_data.$accession."/gene_base_stats/";
my $phasing_data_output_dir = $root_dir_data.$accession."/gene_phasing_data/";

my $working_dir_results = $root_dir_results.$accession."/";
&make_dir($working_dir_results, "if_does_not_exist");

# Get version ...
my $ua = LWP::UserAgent::Determined->new;
$ua->timeout(300);
$ua->timing("10,30,90");
$ua->env_proxy;
my $request = POST $app_server.$app_script, [
					api_key => $api_key,
					todo => 'get_versions'
					];
my $response = $ua->request($request);
my $version_string = "";
if ($response->content =~ /\<\!--version_string_START--\>(.*)\<\!--version_string_END--\>/s) {
	$version_string = $1;
}
#my $version_string =  `perl $a_pl_script --todo=get_versions`;
#print "version_string: ".$version_string."\n";


my $header = <<END;
<html>
<head>
<head>
<link type="text/css" href="web_files/antigens_styles.css" rel="Stylesheet">

<script src="web_files/jquery-1.12.4.min.js">
</script>
<link type="text/css" href="web_files/jquery-ui.css" rel="Stylesheet">
<script src="web_files/jquery-ui.min.js" type="text/javascript">
</script>

<link rel="stylesheet" href="web_files/bootstrap.min.css">
<link rel="stylesheet" href="web_files/bootstrap-theme.min.css">
<script src="web_files/bootstrap.min.js">
</script>

<link type="text/css" rel="stylesheet" href="web_files/jquery.qtip.min.css">
<script src="web_files/jquery.qtip.min.js" type="text/javascript">
</script>

<script src="web_files/jquery.livequery.min.js" type="text/javascript">
</script>

<script src="web_files/bloodcellantigens.js" type="text/javascript">
</script>

<script src="web_files/flotr2.min.js" type="text/javascript">
</script>

<script src="web_files/vis.min.js" type="text/javascript">
</script>
<link type="text/css" href="web_files/vis.min.css" rel="Stylesheet">

<script src="web_files/zebra_cookie.min.js" type="text/javascript">
</script>

</head>
<body style="padding:5px;">
<div class="container" style="margin:0px;">
END

my $footer = <<END;
</div>
</body>
</html>
END

my $summary_div_template = <<END;
<a name="summary"></a>
<div class="centered_div_border">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td align="left" valign="top">
	<span class="red_title">Typing Summary</span>
	<br><br>
	Additional details can be found after the Summary.
</td>
<td align="right">
	<table style="border-collapse: collapse; bgcolor: #000; margin-left:auto; margin-bottom:5px; text-align:center;" border="2" cellpadding="3" cellspacing="0">
	<tr>
		<td class="antigen_type_rare">
			rare
		</td>
		<td class="antigen_type_uncommon">
			uncommon
		</td>
		<td class="antigen_type_average">
			average
		</td>
		<td class="antigen_type_common">
			common
		</td>
		<td class="antigen_type_universal">
			universal
		</td>
	</tr>
	<tr>
		<td>
			0&ndash;5%
		</td>
		<td>
			6&ndash;40%
		</td>
		<td>
			41&ndash;60%
		</td>
		<td>
			61&ndash;94%
		</td>
		<td>
			95&ndash;100%
		</td>
	</tr>
	</table>
	<span class="low_coverage_call">Low Coverage</span>
</td>
</tr>
</table>
<br>
	<INSERT_SUMMARY_TABLES>
</div>

<INSERT_RH_SVG_SECTION_HTML>
END

my $summary_item_template = <<END;
<table>
<tr>
<td nowrap valign="top">
	<font style="font-weight: bold; font-size:18px;"><INSERT_SYSTEM_NUM> <INSERT_SYSTEM_NAME> <a href="#<INSERT_SYSTEM_GENE>"><font color="#0073ea"><i><INSERT_SYSTEM_GENE></i></font></a>: </font>
</td>
<td>
	<INSERT_PREDICTED_PHENOTYPE_TRADITIONAL>
</td>
</tr>
</table>
END


my $phase_info_html = "";
if ($use_experimental_phasing_data ne "") {
	if ($use_experimental_phasing_data eq "no") {
		$include_misphase_in_results = "";
	}
	my $phase_info_html = <<END;
	<div class="col-xs-12 col-sm-6" style="text-align: right;">
		Experimentally phased: $use_experimental_phasing_data<br>
		Misphased included in results: $include_misphase_in_results<br>		
	</div>
END
}

my $combined_prediction_results_template = <<END;
<INSERT_HEADER>
<div class="row" style="max-width:900px; margin-left:-15px; margin-right:-15px; padding-bottom: 10px;">
	<div class="col-xs-12 col-sm-6" style="text-align: left;">
		<!--PHI_START-->
			<INSERT_ACCESSIONS_HTML>
		<!--PHI_END-->
	</div>
	<div class="col-xs-12 col-sm-6" style="text-align: right;">
		Sequence source: $sequence_source<br>
		Instrument type: $instrument_type<br>
		Sequence Type: $sequence_type<br>
		Reference Genome: GRCh$reference_GRCh_genome_build_num
	</div>
	$phase_info_html
</div>
<div style="font-size: 12px;">
	$version_string
</div>
<INSERT_SUMMARY>
<INSERT_DETAILS>
<INSERT_FOOTER>
END


my $ISBT_export_header = "system_num\tsystem_name\tantigen_num\tantigen_name\tantigen_status\tgene_allele_nt_coverage\tphase_groups\n";

my $export_file_combined = "";
my $pre_phase_estimation_antigen_calls_file = "";

my $pred_output_dir = "";
foreach my $system_type (@types_of_antigen_systems) {
	my $system_sub_dir = "";
	my $system_code = "";
	if ($system_type eq "Blood Group System") {
		$system_sub_dir = "RBC_antigens/";
		$system_code = "RBC";
	} elsif ($system_type eq "HPA Gene") {
		$system_sub_dir = "PLT_antigens/";
		$system_code = "PLT";
	} elsif ($system_type eq "HNA Gene") {
			$system_sub_dir = "HNA_antigens/";
			$system_code = "HNA";
	}
	
	&get_web_files($system_sub_dir);
	
	print "Predicting: ".$system_type." ...\n";
	
	$pred_output_dir = $working_dir_results.$system_sub_dir;
	&make_dir($pred_output_dir, "if_does_not_exist");
	
	my $prediction_file_combined = $pred_output_dir.$accession."_Combined_typing.html";
	my $coverage_file_combined = "";
	$export_file_combined = $pred_output_dir.$accession."_Combined_antigen_export.txt";
	$pre_phase_estimation_antigen_calls_file = $pred_output_dir.$accession."_pre_phase_estimation_antigen_calls.txt";
	if ($system_type eq "Blood Group System") {
		$prediction_file_combined = $pred_output_dir.$accession."_Combined_RBC_antigens.html";
		$coverage_file_combined = $pred_output_dir.$accession."_gene_coverage_RBC_antigens.html";
		$export_file_combined = $pred_output_dir.$accession."_Combined_RBC_antigen_export.txt";
	} elsif ($system_type eq "HPA Gene") {
		$prediction_file_combined = $pred_output_dir.$accession."_Combined_PLT_antigens.html";
		$coverage_file_combined = $pred_output_dir.$accession."_gene_coverage_PLT_antigens.html";
		$export_file_combined = $pred_output_dir.$accession."_Combined_PLT_antigen_export.txt";
	} elsif ($system_type eq "HNA Gene") {
		$prediction_file_combined = $pred_output_dir.$accession."_Combined_HNA_antigens.html";
		$coverage_file_combined = $pred_output_dir.$accession."_gene_coverage_HNA_antigens.html";
		$export_file_combined = $pred_output_dir.$accession."_Combined_HNA_antigen_export.txt";
	}
	
	# Get system information from db ...
	#my $sqlstatement = "SELECT system_num, system_name, system_symbol, system_gene_name, system_chr, system_gene_start_by_genomic_pos, system_gene_end_by_genomic_pos from blood_group WHERE system_type = '$system_type' AND hide_system_from_list <> 'yes' ORDER BY `system_num` ASC".";";
	#print "sqlstatement: ".$sqlstatement."<BR>\n";
	#my $sth = $DBH->prepare($sqlstatement);						#prepare and execute SQL statement
	#$sth->execute () || die "Could not execute SQL statement, maybe invalid?";
	
	
	# Get a list of the genes to predict ...
	my @genes_to_use_list = ();
	if (@genes_to_predict_list) {
		foreach my $gene (@genes_to_predict_list) {
			$gene =~ s/^\s+|\s+$//g;
			if ($gene_info{$gene}{'system_type'} eq $system_code) {
				push(@genes_to_use_list, $gene);
			}
		}
	} else {
		foreach my $gene (@gene_list) {
			if ($gene_info{$gene}{'system_type'} eq $system_code) {
				push(@genes_to_use_list, $gene);
			}
		}
	}
	
	my @found_alleles_line_out_array = ();
	my %other_changes_line_out_hash = ();
	my @other_changes_line_out_array_other = ();
	my @other_changes_line_out_array_silent = ();
	my @other_changes_line_out_array_missense = ();
	my @other_change_seq_regions = ("CDS", "intron", "5_prime", "3_prime");
	my @other_changes_gene_name_list = ();
	my $output_C_details = "";
	my $output_C_export = "";
	my $output_C_seq_coverage = "";
	my $summary_predicted_phenotype_traditional = "";
	my %coverage_calc_hash = ();
	my %combined_coverage_calc_hash = ();
	my @min_coverage_cutoff_array = ("10", "20", "30");
	my @genes_typed_list = ();	

	# Go through each gene and get the prediction ...
	foreach my $gene (@genes_to_use_list) {
		my $system_num = $gene_info{$gene}{'system_num'};
		my $system_name = $gene_info{$gene}{'system_name'};
		my $system_symbol = $gene_info{$gene}{'system_symbol'};
		my $system_gene_name = $gene_info{$gene}{'system_gene_name'};
		my $system_chr = $gene_info{$gene}{'system_chr'};
		my $system_gene_start_by_genomic_pos = $gene_info{$gene}{'system_gene_start_by_genomic_pos'};
		my $system_gene_end_by_genomic_pos = $gene_info{$gene}{'system_gene_end_by_genomic_pos'};
		
		# read the the seq ...
		my $fa_file = "";
		my $seq_type = "";
		my $before_prediction_allele_ids_found_file = $pred_output_dir.$accession."_pre_typing_allele_ids_found.txt";
		my $before_typing_structural_changes_found_file = $pred_output_dir.$accession."_pre_typing_structural_gene_changes_found.txt";
		my $base_stats_file = $gene_base_stats_dir.$system_sub_dir.$system_num."_".$system_symbol."-".$system_gene_name."_base_stats.txt";
		my $phasing_data_file = $phasing_data_output_dir.$system_sub_dir.$system_num."_".$system_symbol."-".$system_gene_name."-phasing_data.txt"; 

		if ($accession eq "Historical_Reference") {
			$seq_type = "CDS";
			$fa_file = $gene_seq_dir.$system_num."_".$system_gene_name."-".$accession."-".$seq_type.".fa";
		} elsif ($accession eq "Human_Reference_Genome_(hg19)") {
			if ($system_gene_name eq "ABO") {	# since we had to patched the sequence lets just use CDS since we know for sure it is patched correctly ...
				$seq_type = "CDS";
				$fa_file = $gene_seq_dir.$system_num."_".$system_gene_name."-".$accession."_patched-".$seq_type.".fa";
			} else {
				$seq_type = "gene";
				$fa_file = $gene_seq_dir.$system_num."_".$system_gene_name."-".$accession."-".$seq_type.".fa";
			}
		} else {
			$seq_type = "gene";
			$fa_file = $gene_seq_dir.$system_sub_dir.$system_num."_".$system_symbol."-".$system_gene_name.".fa";
		}
		if (-e $fa_file) {
			push(@genes_typed_list, $system_gene_name);
			push(@other_changes_gene_name_list, $system_gene_name);

			print "Typing: ".$system_num."_".$system_symbol."-".$system_gene_name."\n";
			my $url_content = &get_prediction($system_gene_name, $seq_type, $fa_file, $before_prediction_allele_ids_found_file, $before_typing_structural_changes_found_file, $base_stats_file, $phasing_data_file);
			
			my $predicted_phenotype_traditional = "";
			if ($url_content =~ /\<\!--predicted_traditional_phenotype_section_START--\>(.*)\<\!--predicted_traditional_phenotype_section_END--\>/s) {
				$predicted_phenotype_traditional = $1;
			}
			if ($url_content =~ /\<\!--predicted_null_phenotype_section_START--\>(.*)\<\!--predicted_null_phenotype_section_END--\>/s) {
				$predicted_phenotype_traditional = $predicted_phenotype_traditional.$1;
			}
			if ($system_gene_name eq "ABO" || $system_gene_name eq "FUT3" || $system_gene_name eq "A4GALT") {
				$predicted_phenotype_traditional = "<INSERT_SUMMARY_PHASE_EST_$system_gene_name>";
			}
			
			my $ISBT_export = "";
			if ($url_content =~ /\<\!--predicted_ISBT_export_START--\>(.*)\<\!--predicted_ISBT_export_END--\>/s) {
				$ISBT_export = $1;
				print "ISBT_export:\n".$ISBT_export."\n";
			}
			my $system_name = "";
			if ($url_content =~ /\<\!--system_name_START--\>(.*)\<\!--system_name_END--\>/s) {
				$system_name = $1;
			}
			my $gene_name = "";
			if ($url_content =~ /\<\!--gene_name_START--\>(.*)\<\!--gene_name_END--\>/s) {
				$gene_name = $1;
			}
			my $seq_coverage = "";
			if ($url_content =~ /\<\!--gene_coverage_START--\>(.*)\<\!--gene_coverage_END--\>/s) {
				$seq_coverage = $1;
			}
			$output_C_seq_coverage = $output_C_seq_coverage."<div style=\"font-size:14px;font-weight:bold; margin-bottom:-30px; margin-top:15px;\">".$system_name." "."[<i>".$gene_name."</i>]</div>"."<BR>".$seq_coverage;

			if ($gene_name eq "") {
				print "url_content: ".$url_content."\n";
			}
			

			# get the other changes ...
			my @other_changes_gene_line_out_array_other = ();
			my @other_changes_gene_line_out_array_silent = ();
			my @other_changes_gene_line_out_array_missense = ();
			foreach my $other_change_seq_region (@other_change_seq_regions) {
				@{$other_changes_line_out_hash{$gene_name}{'poorSeq'}{$other_change_seq_region}} = ();
				@{$other_changes_line_out_hash{$gene_name}{'silent'}{$other_change_seq_region}} = ();
				@{$other_changes_line_out_hash{$gene_name}{'missense'}{$other_change_seq_region}} = ();
				@{$other_changes_line_out_hash{$gene_name}{'not_silent_not_missense'}{$other_change_seq_region}} = ();
				
				my @changes_other = ();
				my @changes_silent = ();
				my @changes_missense = ();
				if ($url_content =~ /\<\!--other_change_START-$other_change_seq_region--\>(.*)\<\!--other_change_END-$other_change_seq_region--\>/s) {
					my $changes_in = $1;
					$changes_in =~ s/&gt;/>/gi;
					$changes_in =~ s/; /;/gi;
					#print "changes_in: ".$changes_in."\n";
					my @changes = split(/;/, $changes_in);
					foreach my $change (@changes) {
						if ($change =~ /poorSeq/) {
							push(@{$other_changes_line_out_hash{$gene_name}{'poorSeq'}{$other_change_seq_region}}, $change);
						} elsif ($change =~ /silent/) {
							push(@{$other_changes_line_out_hash{$gene_name}{'silent'}{$other_change_seq_region}}, $change);
						} elsif ($change =~ /missense/) {
							push(@{$other_changes_line_out_hash{$gene_name}{'missense'}{$other_change_seq_region}}, $change);
						} else {
							push(@{$other_changes_line_out_hash{$gene_name}{'not_silent_not_missense'}{$other_change_seq_region}}, $change);
						}
					}
				}
			}

			# get the found alleles data ...
			my $found_alleles_export = "";
			if ($url_content =~ /\<\!--found_alleles_for_export-START--\>(.*)\<\!--found_alleles_for_export-END--\>/s) {
				$found_alleles_export = $1;
				#print "found_alleles_export: ".$found_alleles_export."\n";
			}
			push(@found_alleles_line_out_array, $found_alleles_export);
			
			# get the coverage stats so we can calcuate a combined stat ...
			my @prefix_array = ("gene-", "exon-");
			foreach my $prefix (@prefix_array) {
				foreach my $min_coverage_cutoff (@min_coverage_cutoff_array) {
					my $match = "\<\!--".$min_coverage_cutoff."_".$prefix."base_num_sum_START--\>(.*)\<\!--".$min_coverage_cutoff."_".$prefix."base_num_sum_END--\>";
					if ($url_content =~ /$match/s) {
						$coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_num_sum'} = $1;
						$combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_num_sum'} = $combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_num_sum'} + $1;
					}
					#print "coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_num_sum'}: ".$coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_num_sum'}."\n";
					#
					my $match = "\<\!--".$min_coverage_cutoff."_".$prefix."all_base_num_sum_START--\>(.*)\<\!--".$min_coverage_cutoff."_".$prefix."all_base_num_sum_END--\>";
					if ($url_content =~ /$match/s) {
						$coverage_calc_hash{$min_coverage_cutoff}{$prefix.'all_base_num_sum'} = $1;
						$combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'all_base_num_sum'} = $combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'all_base_num_sum'} + $1;
					}
					#print "coverage_calc_hash{$min_coverage_cutoff}{$prefix.'all_base_num_sum'}: ".$coverage_calc_hash{$min_coverage_cutoff}{$prefix.'all_base_num_sum'}."\n";
					#
					my $match = "\<\!--".$min_coverage_cutoff."_".$prefix."base_coverage_percent_START--\>(.*)\<\!--".$min_coverage_cutoff."_".$prefix."base_coverage_percent_END--\>";
					if ($url_content =~ /$match/s) {
						$coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_coverage_percent'} = $1;
					}
					#print "coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_coverage_percent'}: ".$coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_coverage_percent'}."\n";
				}
				my $match = "\<\!--".$prefix."all_coverage_sum_START--\>(.*)\<\!--".$prefix."all_coverage_sum_END--\>";
				if ($url_content =~ /$match/s) {
					$coverage_calc_hash{'all'}{$prefix.'coverage_sum'} = $1;
					$combined_coverage_calc_hash{'all'}{$prefix.'coverage_sum'} = $combined_coverage_calc_hash{'all'}{$prefix.'coverage_sum'} + $1;
				}
				#print "coverage_calc_hash{'all'}{$prefix.'coverage_sum'}: ".$coverage_calc_hash{'all'}{$prefix.'coverage_sum'}."\n";
				#
				my $match = "\<\!--".$prefix."all_base_num_sum_START--\>(.*)\<\!--".$prefix."all_base_num_sum_END--\>";
				if ($url_content =~ /$match/s) {
					$coverage_calc_hash{'all'}{$prefix.'base_num_sum'} = $1;
					$combined_coverage_calc_hash{'all'}{$prefix.'base_num_sum'} = $combined_coverage_calc_hash{'all'}{$prefix.'base_num_sum'} + $1;
				}
				#print "coverage_calc_hash{'all'}{$prefix.'base_num_sum'}: ".$coverage_calc_hash{'all'}{$prefix.'base_num_sum'}."\n";
				#
				my $match = "\<\!--".$prefix."all_coverage_average_START--\>(.*)\<\!--".$prefix."all_coverage_average_END--\>";
				if ($url_content =~ /$match/s) {
					$coverage_calc_hash{'all'}{$prefix.'coverage_average'} = $1;
				}
				#print "coverage_calc_hash{'all'}{$prefix.'coverage_average'}: ".$coverage_calc_hash{'all'}{$prefix.'coverage_average'}."\n";
			}

			my $seq_coverage = "";
			if ($url_content =~ /\<\!--gene_coverage_START--\>(.*)\<\!--gene_coverage_END--\>/s) {
				$seq_coverage = $1;
			}

# 			my $export_file = $pred_output_dir.$system_num."_".$system_symbol."-".$system_gene_name."__ISBT_export.txt";
# 			open (OUT, ">$export_file");
# 			print OUT $ISBT_export_header;
# 			print OUT $ISBT_export;
# 			close(OUT);
			$output_C_export = $output_C_export.$ISBT_export;
			
			my $summary_item = $summary_item_template;
			$system_num = "";
			$summary_item =~ s/<INSERT_SYSTEM_NUM>/$system_num/gi;
			$summary_item =~ s/<INSERT_SYSTEM_NAME>/$system_name/gi;
			$summary_item =~ s/<INSERT_SYSTEM_GENE>/$system_gene_name/gi;
			$summary_item =~ s/<INSERT_PREDICTED_PHENOTYPE_TRADITIONAL>/$predicted_phenotype_traditional/gi;
			
			if ($predicted_phenotype_traditional ne "") {
				$summary_predicted_phenotype_traditional = $summary_predicted_phenotype_traditional.$summary_item;
			}
# 			my $prediction_file = $pred_output_dir.$system_num."_".$system_symbol."-".$system_gene_name."__prediction.html";
# 			open (OUT, ">$prediction_file");
# 			print OUT $header;
# 			print OUT $url_content;
# 			print OUT $footer;
# 			close(OUT);
			$output_C_details = $output_C_details.$url_content;
		} else {
			print "File not found: ".$fa_file."\n";
		}
	}
	
	#write out the found allele changes.
	my $summary_file = $pred_output_dir.$accession."_alleles_found.txt";
	my $summary_file_temp = $pred_output_dir.$accession."_alleles_found-temp.txt";
	open (OUT, ">$summary_file_temp");
	my $line = $_;
	for my $gene_for (0 .. $#other_changes_gene_name_list) {
		print OUT $other_changes_gene_name_list[$gene_for]."\t".$found_alleles_line_out_array[$gene_for]."\n";
	}
	close(OUT);
	move($summary_file_temp,$summary_file);

	# write out to the other changes file ...
	#
	my $summary_file = $pred_output_dir.$accession."_other_changes_poorSeq.txt";
	my $summary_file_temp = $pred_output_dir.$accession."_other_changes_poorSeq-temp.txt";
	open (OUT, ">$summary_file_temp");
	for my $gene_name (@other_changes_gene_name_list) {
		#print "gene_name: ".$gene_name."\n";
		foreach my $other_change_seq_region (@other_change_seq_regions) {
			#print "other_change_seq_region: ".$other_change_seq_region."\n";
			if (@{$other_changes_line_out_hash{$gene_name}{'poorSeq'}{$other_change_seq_region}}) { 
				print OUT $gene_name."\t".$other_change_seq_region."\t".join(";", @{$other_changes_line_out_hash{$gene_name}{'poorSeq'}{$other_change_seq_region}})."\n";
			}
		}
	}
	close(OUT);
	move($summary_file_temp,$summary_file);
	#
	my $summary_file = $pred_output_dir.$accession."_other_changes_silent.txt";
	my $summary_file_temp = $pred_output_dir.$accession."_other_changes_silent-temp.txt";
	open (OUT, ">$summary_file_temp");
	for my $gene_name (@other_changes_gene_name_list) {
		#print "gene_name: ".$gene_name."\n";
		foreach my $other_change_seq_region ("CDS") {
			#print "other_change_seq_region: ".$other_change_seq_region."\n";
			if (@{$other_changes_line_out_hash{$gene_name}{'silent'}{$other_change_seq_region}}) {
				print OUT $gene_name."\t".$other_change_seq_region."\t".join(";", @{$other_changes_line_out_hash{$gene_name}{'silent'}{$other_change_seq_region}})."\n";
			}
		}
	}
	close(OUT);
	move($summary_file_temp,$summary_file);
	#
	my $summary_file = $pred_output_dir.$accession."_other_changes_missense.txt";
	my $summary_file_temp = $pred_output_dir.$accession."_other_changes_missense-temp.txt";
	open (OUT, ">$summary_file_temp");
	for my $gene_name (@other_changes_gene_name_list) {
		#print "gene_name: ".$gene_name."\n";
		foreach my $other_change_seq_region ("CDS") {
			#print "other_change_seq_region: ".$other_change_seq_region."\n";
			if (@{$other_changes_line_out_hash{$gene_name}{'missense'}{$other_change_seq_region}}) {
				print OUT $gene_name."\t".$other_change_seq_region."\t".join(";", @{$other_changes_line_out_hash{$gene_name}{'missense'}{$other_change_seq_region}})."\n";
			}
		}
	}
	close(OUT);
	move($summary_file_temp,$summary_file);
	#
	my $summary_file = $pred_output_dir.$accession."_other_changes_not_silent_not_missense.txt";
	my $summary_file_temp = $pred_output_dir.$accession."_other_changes_not_silent_not_missense-temp.txt";
	open (OUT, ">$summary_file_temp");
	for my $gene_name (@other_changes_gene_name_list) {
		#print "gene_name: ".$gene_name."\n";
		foreach my $other_change_seq_region (@other_change_seq_regions) {
			#print "other_change_seq_region: ".$other_change_seq_region."\n";
			if (@{$other_changes_line_out_hash{$gene_name}{'not_silent_not_missense'}{$other_change_seq_region}}) {
				print OUT $gene_name."\t".$other_change_seq_region."\t".join(";", @{$other_changes_line_out_hash{$gene_name}{'not_silent_not_missense'}{$other_change_seq_region}})."\n";
			}
		}
	}
	close(OUT);
	move($summary_file_temp,$summary_file);

	# calculate the combined coverage stats ...
	my $coverage_stats_table = "";
	$coverage_stats_table = $coverage_stats_table."<table style=\"border-collapse: collapse; bgcolor: #000; margin-right:auto; margin-bottom:10px; text-align:center;\" border=\"2\" cellpadding=\"3\" cellspacing=\"0\">\n";
	$coverage_stats_table = $coverage_stats_table."<tr><td colspan=\"5\">Combined Coverage Stats for all ".$system_type."s</td></tr>\n";
	$coverage_stats_table = $coverage_stats_table."<tr>\n";
	$coverage_stats_table = $coverage_stats_table."<td>Region</td>\n";
	foreach my $min_coverage_cutoff (@min_coverage_cutoff_array) {
		$coverage_stats_table = $coverage_stats_table."<td>".$min_coverage_cutoff."x</td>\n";
	}
	$coverage_stats_table = $coverage_stats_table."<td>Avg. Coverage</td>\n";
	my @prefix_array = ("gene-", "exon-");
	foreach my $prefix (@prefix_array) {
		$coverage_stats_table = $coverage_stats_table."<tr>\n";
		my $prefix_to_print = $prefix;
		$prefix_to_print =~ s/-//;
		$coverage_stats_table = $coverage_stats_table."<td>$prefix_to_print</td>\n";
		foreach my $min_coverage_cutoff (@min_coverage_cutoff_array) {	
			if ($combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'all_base_num_sum'} > 0) {
				$combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_coverage_percent'} = &round($combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_num_sum'} / $combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'all_base_num_sum'} * 100, 1)."%";
			}
			$coverage_stats_table = $coverage_stats_table.
				"<td>"."\n".
				$combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_num_sum'}."\n".
				"/"."\n".
				$combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'all_base_num_sum'}."\n".
				" = "."<b>"."\n".
				$combined_coverage_calc_hash{$min_coverage_cutoff}{$prefix.'base_coverage_percent'}."\n".
				"</b>"."</td>\n";
		}
		if ($combined_coverage_calc_hash{'all'}{$prefix.'base_num_sum'} > 0) {
			$combined_coverage_calc_hash{'all'}{$prefix.'coverage_average'} = &round($combined_coverage_calc_hash{'all'}{$prefix.'coverage_sum'} / $combined_coverage_calc_hash{'all'}{$prefix.'base_num_sum'}, 0)."x";	
		}
		$coverage_stats_table = $coverage_stats_table.
			"<td>"."\n".
			$combined_coverage_calc_hash{'all'}{$prefix.'coverage_average'}."\n".
			"</td>"."\n";
	}
	$coverage_stats_table = $coverage_stats_table."</table><br>\n";
	

	my $accessions_html = "";
	$accessions_html = $accessions_html."Accession: <span style=\"color: #cf3232; font-weight: bold;\">".$accession."</span><BR>";
	#$accessions_html = $accessions_html."Accession2: ".$accession2."<BR>";
	
	my $summary_div = $summary_div_template;
	$summary_div =~ s/<INSERT_SUMMARY_TABLES>/$summary_predicted_phenotype_traditional/gi;
	
	my $RH_SVG_section_html = "";
	if ($system_type eq "Blood Group System" && $sequence_type ne "SNP") {
		if (grep (/^\QRHD\E$/, @genes_typed_list) || grep (/^\QRHCE\E$/, @genes_typed_list) || grep (/^\QGYPA\E$/, @genes_typed_list) || grep (/^\QGYPB\E$/, @genes_typed_list)) {	# only show if we typed on of these genes ...
			$RH_SVG_section_html = <<END;
<br>
<div class="centered_div_border">
<span class="red_title">RH and MNS System Gene Coverage</span>
<br>
<div>
	<embed src="<INSERT_ACCESSION>_copy_number_plots.svg" type="image/svg+xml">
</div>
</div>
END
		} else {
			$RH_SVG_section_html = "";	
		}
	} else {
		$RH_SVG_section_html = "";	
	}
	$RH_SVG_section_html =~ s/<INSERT_ACCESSION>/$accession/gi;
	$summary_div =~ s/<INSERT_RH_SVG_SECTION_HTML>/$RH_SVG_section_html/gi;
	
	open (OUT_C, ">$coverage_file_combined");
	print OUT_C $header;
	print OUT_C $coverage_stats_table;
	print OUT_C $output_C_seq_coverage;
	print OUT_C $footer;
	close (OUT_C);

	open (OUT_C, ">$export_file_combined");
	print OUT_C $ISBT_export_header;
	print OUT_C $output_C_export;
	close (OUT_C);
	
	if ($system_type eq "Blood Group System") {	
		my $carbo_antigen_prediction_ref = &predict_carbo();
		
		my $ABO_summary = "<span class=\"is_main_variant\">".$carbo_antigen_prediction_ref->{'ABO'}."</span>";
		$summary_div =~ s/<INSERT_SUMMARY_PHASE_EST_ABO>/$ABO_summary/gi;
		my $LE_sumamry = "<span class=\"is_main_variant\">"."Le(a".$carbo_antigen_prediction_ref->{'Le(a)'}."b".$carbo_antigen_prediction_ref->{'Le(b)'}.")"."</span>";
		$summary_div =~ s/<INSERT_SUMMARY_PHASE_EST_FUT3>/$LE_sumamry/gi;
		my $P1PK_summary = "<span class=\"is_main_variant\">"."P1".$carbo_antigen_prediction_ref->{'P1'}."p(k)".$carbo_antigen_prediction_ref->{'p(k)'}."NOR".$carbo_antigen_prediction_ref->{'NOR'}."</span>";
		$summary_div =~ s/<INSERT_SUMMARY_PHASE_EST_A4GALT>/$P1PK_summary/gi;
	}

	# remove the allele network graphs based on run settings ...	
	if ($show_allele_network_graphs eq "no") {
		$output_C_details =~ s/<!--Network_graph-START-->.*?<!--Network_graph-END-->//sgm;
	}
	
	my $output_C = $combined_prediction_results_template;
	$output_C =~ s/<INSERT_HEADER>/$header/gi;
	$output_C =~ s/<INSERT_ACCESSIONS_HTML>/$accessions_html/gi;
	$output_C =~ s/<INSERT_SUMMARY>/$summary_div/gi;
	$output_C =~ s/<INSERT_DETAILS>/$output_C_details/gi;
	$output_C =~ s/<INSERT_FOOTER>/$footer/gi;
	open (OUT_C, ">$prediction_file_combined");
	print OUT_C $output_C;
	close (OUT_C);

	&create_coverage_of_antigen_file($pred_output_dir, $accession);
}

sub call_estimate_antigens_webservice() {
	my ($try_num, $antigen_export_data) = @_;
	
	$try_num++;
	if ($try_num > 10) {
		print "ERROR: Giving up after 10 tries to call webservice\n";
		exit();
	}
	print "Calling call_estimate_antigens_webservice at ".$app_server.$app_script.". Try #".$try_num."\n";
	my $sleep_amount = 60*($try_num-1);
	if ($sleep_amount > 0) {
		print "Sleeping for: ".$sleep_amount." seconds"."\n";
		sleep $sleep_amount;
	}
	
	#open("OUT", ">>md5_phase_estimate.txt");
	#print OUT $accession."\t".""."\t".md5_hex($antigen_export_data)."\n";
	#close(OUT);
	
	my $ua = LWP::UserAgent::Determined->new;
    $ua->timeout(300);
    $ua->timing("10,30,90");
    $ua->env_proxy;
    my $url_content = "";
    my $request = POST $app_server.$app_script, [
    					api_key => $api_key,
    					sample_name => $sample_name,
    					access_code => $access_code,
    					todo => 'estimate_phase_form_process',
    					antigen_export_data => $antigen_export_data,
    					run_timestamp => $run_timestamp
    					];
    my $response = $ua->request($request);
    if ($response->is_success) {
        $url_content = $response->content;
        print "url_content: ".$url_content."\n";
        return $url_content;
    } else {
	    print "WARNING ".$response->status_line."\n";
	    print "Will try again ...\n";
        $url_content = &call_estimate_antigens_webservice($try_num, $antigen_export_data);
        return $url_content;
    }
}

sub predict_carbo () {
	my $antigen_export_data = "";
	open(OUT, ">$pre_phase_estimation_antigen_calls_file");
	open(IN, $export_file_combined);
	while (<IN>) {
		my $line = $_;
		print OUT $line;
		$antigen_export_data = $antigen_export_data.$line;
	}
	close(IN);
	close(OUT);

	my $url_content = &call_estimate_antigens_webservice(0, $antigen_export_data);

	my $carbo_predict_file = $pred_output_dir.$accession."_carbo_antigens.txt";
	open(OUT, ">$carbo_predict_file");
	print OUT $url_content;
	close(OUT);
	
	# read in the carbo antigen info and store in some values to update carbo predictions in previous files when we only had one gene during its creation ...
	my %carbo_antigen_prediction = ();
	my %carbo_antigen_prediction_via_ISBT = ();
	open(IN, $carbo_predict_file);
	while (<IN>) {
		my $line = $_;
		chomp($line);
		if ($line ne "") {
			my @fields = split(/\t/, $line);
			$carbo_antigen_prediction{$fields[0]} = $fields[1];
			print "carbo_antigen_prediction{$fields[0]}: ".$carbo_antigen_prediction{$fields[0]}."\n";
			
			my @ISBT_string_split = split(/\:/, $fields[2]);
			my $ISBT_symbol = $ISBT_string_split[0];
			my @ISBT_antigen_list = split(/,/, $ISBT_string_split[1]);
			foreach my $antigen_prediction (@ISBT_antigen_list) {
				my $antigen = $antigen_prediction;
				$antigen =~ s/-//gi;
				$antigen_prediction =~ s/\d*//gi;
				if ($antigen_prediction eq "") {
					$antigen_prediction = "+";
				}
				$carbo_antigen_prediction_via_ISBT{$ISBT_symbol}{$antigen} = $antigen_prediction;
			}
		}
	}
	close(IN);

	# Update the antigen export file ...
	open(OUT, ">$export_file_combined");
	my @lines = split(/\n/, $antigen_export_data);
	foreach my $line (@lines) {
		my @fields = split(/\t/, $line);
		if ($fields[1] eq "LE" && $fields[2] eq "1") {	# We need to expand the original one LE to two for a and b now that we also know FUT2 ...
			# a
			$fields[2] = "1";
			$fields[3] = "Le(a)";
			$fields[4] = $carbo_antigen_prediction_via_ISBT{$fields[1]}{$fields[2]};
			print OUT join("\t", @fields)."\n";
			# b
			$fields[2] = "2";
			$fields[3] = "Le(b)";
			$fields[4] = $carbo_antigen_prediction_via_ISBT{$fields[1]}{$fields[2]};
			print OUT join("\t", @fields)."\n";
		} elsif ($fields[1] eq "H" && $fields[2] eq "1") {	# We need to expand with antigen name and replace active ...
			$fields[3] = "H";
			$fields[4] = $carbo_antigen_prediction_via_ISBT{$fields[1]}{$fields[2]};
			print OUT join("\t", @fields)."\n";
		} elsif ($fields[1] eq "I" && $fields[2] eq "1") {	# We need to expand with antigen name and replace active ...
			$fields[3] = "I";
			$fields[4] = $carbo_antigen_prediction_via_ISBT{$fields[1]}{$fields[2]};
			print OUT join("\t", @fields)."\n";
		} elsif ($fields[1] eq "GLOB" && $fields[2] eq "3") {	# We need to expand with antigen name and replace active ...
			$fields[3] = "LKE";
			$fields[4] = $carbo_antigen_prediction_via_ISBT{$fields[1]}{$fields[2]};
			print OUT join("\t", @fields)."\n";
		} elsif ($fields[1] eq "GLOB" && $fields[2] eq "4") {	# We need to expand with antigen name and replace active ...
			$fields[3] = "PX2";
			$fields[4] = $carbo_antigen_prediction_via_ISBT{$fields[1]}{$fields[2]};
			print OUT join("\t", @fields)."\n";
		} elsif ($fields[1] eq "FORS" && $fields[2] eq "1") {	# We need to expand with antigen name and replace active ...
			$fields[3] = "Fs";
			$fields[4] = $carbo_antigen_prediction_via_ISBT{$fields[1]}{$fields[2]};
			print OUT join("\t", @fields)."\n";
		} elsif ($carbo_antigen_prediction_via_ISBT{$fields[1]}{$fields[2]}) {
			$fields[4] = $carbo_antigen_prediction_via_ISBT{$fields[1]}{$fields[2]};
			print OUT join("\t", @fields)."\n";
			if ($fields[1] eq "ABO" && $fields[2] eq "2") {	# lets add after this ABO:3 for AB ...
				$fields[2] = "3";
				$fields[3] = "AB";
				if ($carbo_antigen_prediction_via_ISBT{'ABO'}{'1'} eq "+" && $carbo_antigen_prediction_via_ISBT{'ABO'}{'2'} eq "+") {	# we are AB ...
					$fields[4] = "+";
				} else {
					$fields[4] = "-";
				}
				print OUT join("\t", @fields)."\n";
			}
		} elsif ($fields[2] =~ /^0/) {	# lets skip these since they were just placeholders for carbo allele combination
		} else {
			print OUT $line."\n";
		}
	}
	close(OUT);
	
	return \%carbo_antigen_prediction;
}


sub create_coverage_of_antigen_file () {
	my ($pred_output_dir, $accession) = @_;
	my %coverage_order_via_nt_type = ("A"=>"0", "G"=>"1", "C"=>"2", "T"=>"3", "del"=>"4", "ins"=>"5");
		
	print $accession."\n";
	
	my $alleles_found_filename = $pred_output_dir.$accession."_alleles_found.txt";
	my $coverage_for_antigen_filename = $pred_output_dir.$accession."_coverage_for_antigens.txt";
	
	print "Reading in: ".$alleles_found_filename."\n";
	open (IN, $alleles_found_filename);
	open (OUT, ">$coverage_for_antigen_filename");
	print "Creating: ".$coverage_for_antigen_filename."\n";
	print OUT "antigen_typing\tgenomic_changes\tcoverage_for_changes\tA_G_C_T_del_ins_coverages\ttotal_coverage\n";
	while (<IN>) {
		my $line = $_;
		chomp($line);
		my @alleles = split(/;/, $line);
		foreach my $allele (@alleles) {
			my @fields = split(/\|/, $allele);
			my $ISBT = $fields[1];
			my @chr_nt_found_split = split(/,/, $fields[4]);
			my @coverage_record_split = split(/,/, $fields[5]);
			
			# get the coverage for the nt_type found in the allele
			my @coverage_total_list = ();
			my @allele_coverage_list = ();
			for my $index (0 .. $#coverage_record_split) {
				my @coverage_fields = split(/:/, $coverage_record_split[$index]);
				my $coverage_total = "";
				foreach my $coverage_field (@coverage_fields) {
					$coverage_total = $coverage_total + $coverage_field;
				}
				push(@coverage_total_list , $coverage_total);
				my $chr_nt = $chr_nt_found_split[$index];
				if ($chr_nt =~ /\:\d*(.*)/) {
					my $nt_type = $1;
					push(@allele_coverage_list, $coverage_fields[$coverage_order_via_nt_type{$nt_type}]);
				}
			}
			
			print OUT $ISBT."\t".join("|", @chr_nt_found_split)."\t".join("|", @allele_coverage_list)."\t".join("|", @coverage_record_split)."\t".join("|", @coverage_total_list)."\n";
		}
	}
	close(IN);
	close(OUT);
}

##### Functions ...
sub call_typing_webservice() {
	my ($try_num, $system_gene_name, $seq_type, $seq_in, $before_prediction_allele_ids_found, $before_prediction_structural_changes_found, $base_stats_per_pos, $nt_base_coverage_cutoff_for_call, $nt_base_coverage_cutoff_low_coverage_call, $phasing_data) = @_;
	
	$try_num++;
	if ($try_num > 10) {
		print "ERROR: Giving up after 10 tries to call webservice\n";
		exit();
	}
	print "Calling call_typing_webservice at ".$app_server.$app_script.". Try #".$try_num."\n";
	my $sleep_amount = 60*($try_num-1);
	if ($sleep_amount > 0) {
		print "Sleeping for: ".$sleep_amount." seconds"."\n";
		sleep $sleep_amount;
	}
	
	#open("OUT", ">>md5_seq_in.txt");
	#print OUT $accession."\t".$system_gene_name."\t".md5_hex($seq_in)."\n";
	#close(OUT);
	
	my $ua = LWP::UserAgent::Determined->new;
    $ua->timeout(300);
    $ua->timing("10,30,90");
    $ua->env_proxy;
    my $url_content = "";
    my $request = POST $app_server.$app_script, [
    					api_key => $api_key,
    					sample_name => $sample_name,
    					access_code => $access_code,
    					todo => 'predict_antigen_form_process',
    					predict_seq_type => $seq_type,
    					system_gene_name => $system_gene_name,
    					seq_in => $seq_in,
    					before_prediction_allele_ids_found => $before_prediction_allele_ids_found,
    					before_prediction_structural_changes_found => $before_prediction_structural_changes_found,
    					base_stats_per_pos => encode_base64(compress($base_stats_per_pos)),
    					phasing_data => $phasing_data,
    					include_misphase_in_results => $include_misphase_in_results,
    					base_stats_compressed => 'yes',
						nt_base_coverage_cutoff_for_call => $nt_base_coverage_cutoff_for_call,
						nt_base_coverage_cutoff_low_coverage_call => $nt_base_coverage_cutoff_low_coverage_call,
						no_coverage_plot => $no_coverage_plot,
						show_coverage_table_regions_list => $show_coverage_table_regions_list,
						run_timestamp => $run_timestamp
    					];
    my $response = $ua->request($request);
    if ($response->is_success) {
        $url_content = $response->content;
        #print $url_content;
        return $url_content;
    } else {
	    print "WARNING ".$response->status_line."\n";
	    print "Will try again ...\n";
        $url_content = &call_typing_webservice($try_num, $system_gene_name, $seq_type, $seq_in, $before_prediction_allele_ids_found, $base_stats_per_pos, $nt_base_coverage_cutoff_for_call, $nt_base_coverage_cutoff_low_coverage_call);
        return $url_content;
    }
}


sub get_prediction () {
	my ($system_gene_name, $seq_type, $fa_file, $before_typing_allele_ids_found_file, $before_typing_structural_changes_found_file, $base_stats_file, $phasing_data_file) = @_;
	
	print "Getting seq from: ".$fa_file."\n";
	open my $fh, '<', $fa_file or die "error opening $fa_file: $!";
	my $seq_in = do { local $/; <$fh> }; 
	
	my $before_prediction_allele_ids_found = "";
	if (-e $before_typing_allele_ids_found_file) {
		print "Getting before typing allele ids found from: ".$before_typing_allele_ids_found_file."\n";
		open my $fh, '<', $before_typing_allele_ids_found_file or die "error opening $before_typing_allele_ids_found_file: $!";
		$before_prediction_allele_ids_found = do { local $/; <$fh> };
	} else {
		print "Did NOT find before typing allele ids file: ".$before_typing_allele_ids_found_file."\n";
	}
	
	my $before_prediction_structural_changes_found = "";
	if (-e $before_typing_structural_changes_found_file) {
		print "Getting before typing structural changes found from: ".$before_typing_structural_changes_found_file."\n";
		open my $fh, '<', $before_typing_structural_changes_found_file or die "error opening $before_typing_structural_changes_found_file: $!";
		$before_prediction_structural_changes_found = do { local $/; <$fh> };
	} else {
		print "Did NOT find before typing structural changes  file: ".$before_typing_structural_changes_found_file."\n";
	}
	
	my $base_stats_per_pos = "";
	if (-e $base_stats_file && $sequence_type ne "SNP") {
		print "Getting base stats from: ".$base_stats_file."\n";
		open my $fh, '<', $base_stats_file or die "error opening $base_stats_file: $!";
		$base_stats_per_pos = do { local $/; <$fh> };
	} else {
		print "Did NOT find a base stats file: ".$base_stats_file."\n";
	}
	
	my $phasing_data = "";
	if ($use_experimental_phasing_data eq "yes") {
		if (-e $phasing_data_file) {
			print "Getting phasing data from: ".$phasing_data_file."\n";
			open my $fh, '<', $phasing_data_file or die "error opening $phasing_data_file: $!";
			$phasing_data = do { local $/; <$fh> };
			#print "phasing_data: ".$phasing_data."\n";
		} else {
			print "Did NOT find phasing data file: ".$phasing_data_file."\n";
		}
	}
	
	my $url_content = "";
    my $url_content = &call_typing_webservice(0, $system_gene_name, $seq_type, $seq_in, $before_prediction_allele_ids_found, $before_prediction_structural_changes_found, $base_stats_per_pos, $nt_base_coverage_cutoff_for_call, $nt_base_coverage_cutoff_low_coverage_call, $phasing_data);
    
    return $url_content;
}

sub get_web_files () {
	my ($system_sub_dir) = @_;
	my $pred_output_dir = $working_dir_results.$system_sub_dir;
	&make_dir($pred_output_dir, "if_does_not_exist");
	$pred_output_dir = $pred_output_dir."/"."web_files";
	&make_dir($pred_output_dir, "if_does_not_exist");

	my @urls_to_get = 	(
							$app_server.$website_root_html_dir."antigens_styles.css",
							$app_server.$website_root_html_dir."jQuery/jquery.livequery-1.1.1/jquery.livequery.min.js",
							$app_server.$website_root_html_dir."jQuery/bloodcellantigens/bloodcellantigens.js",
							$app_server.$website_root_html_dir."jQuery/Flotr2/flotr2.min.js",
							$app_server.$website_root_html_dir."jQuery/vis/vis.min.js",
							$app_server.$website_root_html_dir."jQuery/vis/vis.min.css",
							$app_server.$website_root_html_dir."jQuery/zebra_cookie/zebra_cookie.min.js",
							"https://code.jquery.com/jquery-1.12.4.min.js",
							"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",
							"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js",
							"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",
							"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css",
							"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js",
							"https://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.css",
							"https://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.js"
						);
	
	foreach my $url (@urls_to_get) { 
		if ($url =~ /^.*\/(.*)$/) {
			my $file = $1;
			my $file_out = $pred_output_dir."/".$file;
			#if (!(-e $file_out)) {
				print "Downloading: ".$url." --> ".$file."\n";
			    
		    	my $url_content = &get_url($url);
				open (OUT, ">$file_out");
				print OUT $url_content;
				close(OUT);		
			#}    
		}
	}
}

sub get_url() {
	my ($url) = @_;
	my $url_content = "";
	print "Downloading: ".$url."\n";
    my $ua = LWP::UserAgent::Determined->new;
    $ua->timeout(300);
    $ua->env_proxy;
    my $response = $ua->get($url);
	die "Error\n ", $response->status_line, "\n Aborting" unless $response->is_success;
    if ($response->is_success) {
        $url_content = $response->content;
    } else {
        print "ERROR no response";
        exit();
    }
    
    return $url_content;
}

sub make_dir () {
    my ($dir_to_make, $make_condition) = @_;
    
    if ($make_condition eq "if_does_not_exist") {
        if (!(-e $dir_to_make)) {
        	mkdir ($dir_to_make);
        }
    }
}
