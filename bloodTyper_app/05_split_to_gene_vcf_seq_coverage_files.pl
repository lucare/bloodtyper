#!/usr/bin/perl
#!C:/Perl/bin/perl.exe

#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################

use Fcntl qw(:DEFAULT :flock);
use strict;
use DBI;
use Getopt::Long;
use Data::Dumper;
use FindBin;
use lib $FindBin::Bin.'/my_perl_modules';
use My::wgs_pred_common;

# can read about vcf files 
# http://gatkforums.broadinstitute.org/discussion/1268/how-should-i-interpret-vcf-files-produced-by-the-gatk

use lib './my_perl_modules';
use My::wgs_pred_common;

my $accession_cleaned_cmdline = "";
my $root_data_dir_cmd_line = "";
my $root_results_dir_cmd_line = "";
my $use_experimental_phasing_data = "";
my $cmdline_result = GetOptions 	(
							"accession_cleaned=s" => \$accession_cleaned_cmdline,
							"root_data_dir=s" => \$root_data_dir_cmd_line,
							"root_results_dir=s" => \$root_results_dir_cmd_line,
							"sequence_source=s" => \$sequence_source,
							"instrument_type=s" => \$instrument_type,
							"sequence_type=s" => \$sequence_type,
							"antigen_systems=s" => \$antigen_systems,
							"genes_to_predict=s" => \$genes_to_predict,
							"probes_to_skip_filename=s" => \$probes_to_skip_filename,
							"probes_to_force_call_filename=s" => \$probes_to_force_call_filename,
							"use_experimental_phasing_data=s" => \$use_experimental_phasing_data,
						);
print "Sequence source: ".$sequence_source."\n";
print "Instrument type: ".$instrument_type."\n";
print "Sequence type: ".$sequence_type."\n";
my @antigen_system_list = split(/,/, $antigen_systems);
print "Antigen Systems: ".join(",", @antigen_system_list)."\n";
my @genes_to_predict_list = split(/,/, $genes_to_predict);
print "genes_to_predict: ".join(",", @genes_to_predict_list)."\n";
print "root_data_dir_cmd_line: ".$root_data_dir_cmd_line."\n";
print "root_results_dir_cmd_line: ".$root_results_dir_cmd_line."\n";
print "use_experimental_phasing_data: ".$use_experimental_phasing_data."\n";

&get_accession_to_use($accession_cleaned_cmdline, $root_data_dir_cmd_line, $root_results_dir_cmd_line);


my $gene_vcf_dir = $root_dir_data.$accession."/gene_vcf/";
my $gene_seq_dir = $root_dir_data.$accession."/gene_seq/";
my $gene_base_stats_dir = $root_dir_data.$accession."/gene_base_stats/";
my $gene_phasing_data_dir = $root_dir_data.$accession."/gene_phasing_data/";
my $pred_output_dir_root = $root_dir_results.$accession."/";
&make_dir($pred_output_dir_root, "if_does_not_exist");

&make_dir($gene_vcf_dir, "if_does_not_exist");
&make_dir($gene_seq_dir, "if_does_not_exist");
&make_dir($gene_base_stats_dir, "if_does_not_exist");
if ($use_experimental_phasing_data eq "yes") {
	&make_dir($gene_phasing_data_dir, "if_does_not_exist");
}

my $vcf_file = $root_dir_data.$accession."/raw_data/".$accession."_bld_type.vcf";


# read in ABO patch info ...
my $patch_pos_dict_file = $pipeline_supporting_files_dir."h19_unpatched_vs_patched_dict.csv";
print "Reading in patch for ABO hg19: ".$patch_pos_dict_file."\n";
my %patched_gene_pos_via_genomic_pos = ();
open(IN, $patch_pos_dict_file);
my $header = <IN>;
while (<IN>) {
	my $line = $_;
	chomp($line);
	my @fields = split (",", $line);
	$patched_gene_pos_via_genomic_pos{$fields[2].":".$fields[3]} = $fields[7];
	#print "patched_gene_pos_via_genomic_pos{$fields[2].\":\".$fields[3]}: ".$patched_gene_pos_via_genomic_pos{$fields[2].":".$fields[3]}."\n";
}
close(IN);

# Read in probes to skip ...
print "Reading in probes to skip file: ".$probes_to_skip_filename."\n";
my @probes_to_skip_list = ();
open(IN, $probes_to_skip_filename);
while (<IN>) {
	my $line = $_;
	chomp($line);
	my @fields = split (/\t/, $line);
	push(@probes_to_skip_list, $fields[0]);
}
close(IN);
print "probes_to_skip_list: ".join(", ", @probes_to_skip_list)."\n";

# Get a list of the genes to predict ...
my @genes_to_use_list = ();
if (@genes_to_predict_list) {
	foreach my $gene (@genes_to_predict_list) {
		$gene =~ s/^\s+|\s+$//g;
		push(@genes_to_use_list, $gene);
	}
} else {
	foreach my $gene (@gene_list) {
		if (grep (/^\Q$gene_info{$gene}{'system_type'}\E$/, @antigen_system_list)) {
			push(@genes_to_use_list, $gene);
		}
	}
}
print "genes_to_use_list: ".join("\t", @genes_to_use_list)."\n";

# Read in the vcf ...
my %vcf_lines_via_gene_name = ();
print "Reading in vcf file: ".$vcf_file."\n";
if (-e $vcf_file) {
	open (IN, $vcf_file);
	while (<IN>) {
		my $line = $_;
		if (!($line =~ /^#/)) {
			my @fields = split(/\s{1,}/, $line);
			my $chr = $fields[0];
			$chr =~ s/^chr//gi;	# just in case the inpit has chr we will remove it to be consistent
			my $current_pos = $fields[1];
			my $probenames = $fields[2];
			my $nt_ref = $fields[3];
			my $nt_alt = $fields[4];
			my $score = $fields[6];
			my $GT = $fields[9];
			my $gene = $gene_via_genomic_pos{$chr.":".$current_pos};
			my @current_probe_list = split(/;/, $probenames);

			$line =~ s/^chr//gi;	# just in case the inpit has chr we will remove it to be consistent

			my $skip = 0;
			foreach my $current_probe (@current_probe_list) {
				if (grep (/^\Q$current_probe\E$/, @probes_to_skip_list)) {	# see if this is on the to skip list ...
					$skip = 1;
					print "Skipping probe: ".$current_probe."\n";
				}	
			}

			if ($skip ne "1") {	# push if we are not skipping ...
				push(@{$vcf_lines_via_gene_name{$gene}}, $line);
			}
		}
	}
}

foreach my $gene (@genes_to_use_list) {
	if (@{ $vcf_lines_via_gene_name{$gene} // [] }) {  
		my $system_sub_dir = "";
		if ($gene_info{$gene}{'system_type'} eq "RBC") {
			$system_sub_dir = "RBC_antigens/";
		} elsif ($gene_info{$gene}{'system_type'} eq "PLT") {
			$system_sub_dir = "PLT_antigens/";
		} elsif ($gene_info{$gene}{'system_type'} eq "HNA") {
			$system_sub_dir = "HNA_antigens/";
		}
		my $vcf_output_dir = $gene_vcf_dir.$system_sub_dir;
		&make_dir($vcf_output_dir, "if_does_not_exist");
		my $vcf_gene_file = $vcf_output_dir.$gene_info{$gene}{'system_num'}."_".$gene_info{$gene}{'system_symbol'}."-".$gene.".vcf";
		#print "Making gene vcf file: ".$vcf_gene_file."\n";
		print "Making: ".$vcf_gene_file."\n";
		open (OUT, ">$vcf_gene_file");
		print OUT join("", @{$vcf_lines_via_gene_name{$gene}});
		close(OUT);
	}
}

# create some dirs ..
foreach my $gene (@genes_to_use_list) {
	if (@{ $vcf_lines_via_gene_name{$gene} // [] }) {  
		my $system_sub_dir = "";
		if ($gene_info{$gene}{'system_type'} eq "RBC") {
			$system_sub_dir = "RBC_antigens/";
		} elsif ($gene_info{$gene}{'system_type'} eq "PLT") {
			$system_sub_dir = "PLT_antigens/";
		} elsif ($gene_info{$gene}{'system_type'} eq "HNA") {
			$system_sub_dir = "HNA_antigens/";
		}
		
		my $pred_output_dir = $pred_output_dir_root.$system_sub_dir;
		&make_dir($pred_output_dir, "if_does_not_exist");
	}
}


my %seq = ();
my %homo_or_het_data = ();
foreach my $gene (@genes_to_use_list) {
	my $system_sub_dir = "";
	if ($gene_info{$gene}{'system_type'} eq "RBC") {
		$system_sub_dir = "RBC_antigens/";
	} elsif ($gene_info{$gene}{'system_type'} eq "PLT") {
		$system_sub_dir = "PLT_antigens/";
	} elsif ($gene_info{$gene}{'system_type'} eq "HNA") {
			$system_sub_dir = "HNA_antigens/";
	}
	my $vcf_output_dir = $gene_vcf_dir.$system_sub_dir;

	my $vcf_gene_file = $vcf_output_dir.$gene_info{$gene}{'system_num'}."_".$gene_info{$gene}{'system_symbol'}."-".$gene.".vcf";
	open(IN, $vcf_gene_file);
	if (-e $vcf_gene_file) {  
		my %has_del = ();
		while (<IN>) {
			my $line = $_;
			if (!($line =~ /^#/)) {
				#print $line."\n";
				#chr7	142638201	.	T	.	.	PASS	END=142638383;BLOCKAVG_min30p3a	GT:DP:GQX:MQ	0/0:89:99:254
				#chr7	142639321	.	T	C	883.01	PASS	BaseQRankSum=0.630;DP=60;Dels=0.00;FS=3.886;HaplotypeScore=5.5431;MLEAC=1;MLEAF=0.500;MQ=254;MQ0=0;MQRankSum=-1.800;QD=14.72;ReadPosRankSum=0.541;SB=-3.280e+02	GT:AD:DP:GQ:PL:MQ:GQX	0/1:32,28:60:99:913,0,1074:254:99
				#4	145041707	.	C	.	.	.	DP=21;MQ=0.00;MQ0=21	GT	./.
				#9	136131056	rs56392308	CG	C	807.73	.	AC=2;AF=1.00;AN=2;DB;DP=25;FS=0.000;HaplotypeScore=12.7263;MLEAC=2;MLEAF=1.00;MQ=58.14;MQ0=0;QD=32.31;RPA=3,2;RU=G;STR	GT:AD:DP:GQ:PL	1/1:0,21:25:63:845,63,0

				#print "-----\n";

				my @fields = split(/\s{1,}/, $line);
				my $chr = $fields[0];
				my $current_pos = $fields[1];
				my $nt_ref = $fields[3];
				my $nt_alt = $fields[4];
				my $score = $fields[6];
				my $GT = $fields[9];
				my @GT_split = split(/:/, $GT);
				
				my $homo_or_het = "";
				if ($GT_split[0] eq "0/0") {	# homo ref ...
					$homo_or_het = "homo";
				} elsif ($GT_split[0] eq "1/1") {	# homo alternate ...
					$homo_or_het = "homo";
				} elsif ($GT_split[0] eq "0/1") {	# hetero ref and alternate ...
					$homo_or_het = "het";
				} elsif ($GT_split[0] eq "./.") {	# hetero ref and alternate ...
					$homo_or_het = "het";
				}
				$homo_or_het_data{$gene_info{$gene}{'system_type'}}{$gene}{$homo_or_het}++;
				
				#print "current_pos: ".$current_pos."\n";
				if ($gene_info{$gene}{'gene_direction'} eq "r") {
					$nt_ref = &flip_base($nt_ref);
					$nt_alt = &flip_base($nt_alt);
				}
				if (length($nt_alt) > 1) {	# we have an insertion ...
					#print "current_pos: ".$current_pos."\n";
					my $nt_alt_with_insert = "";
					if ($gene_info{$gene}{'gene_direction'} eq "f") {
						my @nt_split = split(//, $nt_alt);
						for my $for (0 .. $#nt_split) {
							if ($for eq 0) {
								$nt_alt_with_insert = $nt_alt_with_insert.$nt_split[$for]."[";
							} elsif ($for eq $#nt_split) {
								$nt_alt_with_insert = $nt_alt_with_insert.$nt_split[$for]."]";
							} else {
								$nt_alt_with_insert = $nt_alt_with_insert.$nt_split[$for];
							}
						}
					} elsif ($gene_info{$gene}{'gene_direction'} eq "r") {
						my @nt_split = split(//, $nt_alt);
						for my $for (0 .. $#nt_split) {
							if ($for eq 0) {
								$nt_alt_with_insert = $nt_alt_with_insert."[".$nt_split[$for];
							} elsif ($for eq $#nt_split) {
								$nt_alt_with_insert = $nt_alt_with_insert."]".$nt_split[$for];
							} else {
								$nt_alt_with_insert = $nt_alt_with_insert.$nt_split[$for];
							}
						}
					}
					$seq{$chr.":".$current_pos} = $nt_alt_with_insert;
					#$seq{$chr.":".$current_pos} = "?";
					#print "seq{$chr:$current_pos}: ".$seq{$chr.":".$current_pos}."\n";
					$nt_alt = $nt_alt_with_insert;
				} elsif (length($nt_ref) > 1) {	# we have a deletion ...
					my $del_length = length($nt_ref) - length($nt_alt);
					my @nt_ref_split = split(//, $nt_ref);
					for my $for_del (1 .. $del_length) {
						my $del_pos = $current_pos+$for_del;
						$has_del{$chr.":".$current_pos} = $GT_split[0];
						if ($GT_split[0] eq "1/1") {	# homo alternate ...
							$seq{$chr.":".$del_pos} = "-";
						} elsif ($GT_split[0] eq "0/1") {	# hetero ref and alternate ...
							my $ref_for_del = "";
							if ($gene_info{$gene}{'gene_direction'} eq "f") { 
								$ref_for_del = $nt_ref_split[$for_del];
							} elsif ($gene_info{$gene}{'gene_direction'} eq "r") {
								$ref_for_del = $nt_ref_split[length($nt_ref)-$for_del-1];
							}
							$seq{$chr.":".$del_pos} = "(".$ref_for_del."|"."-".")";
						}
						#print "seq{$chr:$del_pos}: ".$seq{$chr.":".$del_pos}."\n";
					}
					#$seq{$chr.":".$current_pos} = "?";
				}
				# fix for ABO pos261 deletion issue 
				# O / B
				#9	136132908	.	T	.	127.23	.	AN=2;DP=35;MQ=59.65;MQ0=0	GT:DP	0/0:35
				#9	136132908	rs8176719	T	TC	581.73	.	AC=1;AF=0.500;AN=2;BaseQRankSum=0.745;DB;DP=35;FS=1.387;HaplotypeScore=109.1432;MLEAC=1;MLEAF=0.500;MQ=59.65;MQ0=0;MQRankSum=0.248;QD=16.62;ReadPosRankSum=0.679	GT:AD:DP:GQ:PL	0/1:19,16:35:99:619,0,756

				#print "-----\n";

				if ($chr eq "9" && $current_pos eq "136132908") {
					if ($nt_alt eq "G") {	# Add the A if BoodArray ... GATK give [G]A, but the BloodArray give G.
						$nt_alt = "[G]A";
					}
					if ($GT_split[0] eq "0/0") {	# homo ref ... 
						$seq{$chr.":".$current_pos} = "-".$nt_ref;
					} elsif ($GT_split[0] eq "1/1") {	# homo alternate ...
						if ($nt_alt eq "[G]A") {
							$nt_alt =~ s/\[//g;
							$nt_alt =~ s/\]//g;
							$seq{$chr.":".$current_pos} = $nt_alt;
						}
					} elsif ($GT_split[0] eq "0/1") {	# hetero ref and alternate ...
						if ($nt_alt eq "[G]A") {
						 	$nt_alt =~ s/\[//g;
							$nt_alt =~ s/\]//g;
							$seq{$chr.":".$current_pos} = "("."-"."|"."G".")A";
						}
					} elsif ($GT_split[0] eq "./.") {	# hetero ref and alternate ...
						$seq{$chr.":".$current_pos} = "P";
					}
				} else {	# treat it as normal ...
					if ($has_del{$chr.":".$current_pos} eq "") {	# we do not have a deletion ... we directly deal with these above.
						#$GT_split[0] = $has_del{$chr.":".$current_pos};	# we stored the GT_split for the deletion before so that we can override this is del is homo or hetero
						#$nt_alt = "-";
						#print "has_del{$chr:$current_pos}: ".$has_del{$chr.":".$current_pos}."\n";
						if (!($seq{$chr.":".$current_pos} =~ /\-/) && !($seq{$chr.":".$current_pos} =~ /\[/)) {	# do not override previously stored deletions and insertions ...
							#print $GT_split[0]."\n";
							if ($GT_split[0] eq "0/0") {	# homo ref ... 
								$seq{$chr.":".$current_pos} = $nt_ref;
							} elsif ($GT_split[0] eq "1/1") {	# homo alternate ...
								$seq{$chr.":".$current_pos} = $nt_alt;
							} elsif ($GT_split[0] eq "0/1") {	# hetero ref and alternate ...
								$seq{$chr.":".$current_pos} = "(".$nt_ref."|".$nt_alt.")";
							} elsif ($GT_split[0] eq "./.") {	# hetero ref and alternate ...
								$seq{$chr.":".$current_pos} = "P";
							}
							#print "seq{$chr:$current_pos}: ".$seq{$chr.":".$current_pos}."\n";
						}
					}
				}
				#} else {	# additional variant for this genomic coord (ie there is a heterozygous insertion like ABO chr9:136132908) though the above should have caputured this ...
				#	print "DUPLICATE: seq{$chr:$current_pos}: ".$seq{$chr.":".$current_pos}."\n";
				#}
				#print "seq{$chr:$current_pos}: ".$seq{$chr.":".$current_pos}."\n";
			}
		}
	}
	close(IN);
}

# get gene het percentage ...
my @system_types = ();
my %data_to_write_het_percentage = ();
foreach my $gene (@genes_to_use_list) {
	my $total = $homo_or_het_data{$gene_info{$gene}{'system_type'}}{$gene}{'homo'} + $homo_or_het_data{$gene_info{$gene}{'system_type'}}{$gene}{'het'};
	my $het_percent = 0;
	if ($total > 0) {
		$het_percent = &round($homo_or_het_data{$gene_info{$gene}{'system_type'}}{$gene}{'het'} / $total *100, 2);
	}
	push(@{$data_to_write_het_percentage{$gene_info{$gene}{'system_type'}}{'header'}}, $gene);
	push(@{$data_to_write_het_percentage{$gene_info{$gene}{'system_type'}}{'het_precent'}}, $het_percent);
	
	if (!(grep (/^\Q$gene_info{$gene}{'system_type'}\E$/, @system_types))) {
		push(@system_types, $gene_info{$gene}{'system_type'});
	}
}

# get the genomic to gene pos mapping ...
my %gene_pos_via_genomic_pos = ();
foreach my $gene (@genes_to_use_list) {
	my $system_gene_start_by_genomic_pos = $gene_info{$gene}{'system_gene_start_by_genomic_pos'};
	my $system_gene_end_by_genomic_pos = $gene_info{$gene}{'system_gene_end_by_genomic_pos'};
	my $chr = $gene_info{$gene}{'system_chr'};
	$chr =~ s/chr//g;
	if ($gene_info{$gene}{'gene_direction'} eq "f") {
		my $gene_pos = 0;
		for (my $pos = $system_gene_start_by_genomic_pos; $pos <= $system_gene_end_by_genomic_pos; $pos = $pos + 1) {
			$gene_pos++;
			$gene_pos_via_genomic_pos{$chr.":".$pos} = $gene_pos;
			#print "gene_pos_via_genomic_pos{$chr.\":\".$pos}: ".$gene_pos_via_genomic_pos{$chr.":".$pos}."\n";
 		}
	} elsif ($gene_info{$gene}{'gene_direction'} eq "r") {
		my $gene_pos = 0;
		for (my $pos = $system_gene_start_by_genomic_pos; $pos >= $system_gene_end_by_genomic_pos; $pos = $pos - 1) {
			$gene_pos++;
			$gene_pos_via_genomic_pos{$chr.":".$pos} = $gene_pos;
			#print "gene_pos_via_genomic_pos{$chr.\":\".$pos}: ".$gene_pos_via_genomic_pos{$chr.":".$pos}."\n";
		}
	}
}

# Phase ...
if ($use_experimental_phasing_data eq "yes") {
	foreach my $gene (@genes_to_use_list) {
		my $system_sub_dir = "";
		if ($gene_info{$gene}{'system_type'} eq "RBC") {
			$system_sub_dir = "RBC_antigens/";
		} elsif ($gene_info{$gene}{'system_type'} eq "PLT") {
			$system_sub_dir = "PLT_antigens/";
		} elsif ($gene_info{$gene}{'system_type'} eq "HNA") {
				$system_sub_dir = "HNA_antigens/";
		}
		my $phasing_data_output_dir = $gene_phasing_data_dir.$system_sub_dir;
		&make_dir($phasing_data_output_dir, "if_does_not_exist");
		
		my $phasing_data_file = $phasing_data_output_dir.$gene_info{$gene}{'system_num'}."_".$gene_info{$gene}{'system_symbol'}."-".$gene."-phasing_data.txt";
		print "Phasing ...\n";
		my $genomic_start_pos = "";
		my $genomic_end_pos = "";
		if ($gene_info{$gene}{'gene_direction'} eq "f") {
			$genomic_start_pos = $gene_info{$gene}{'system_gene_start_by_genomic_pos'};
			$genomic_end_pos = $gene_info{$gene}{'system_gene_end_by_genomic_pos'};
		} elsif ($gene_info{$gene}{'gene_direction'} eq "r") {
			$genomic_start_pos = $gene_info{$gene}{'system_gene_end_by_genomic_pos'};
			$genomic_end_pos = $gene_info{$gene}{'system_gene_start_by_genomic_pos'};
		}
		my $chr = $gene_info{$gene}{'system_chr'};

		my $vcf_output_dir = $gene_vcf_dir.$system_sub_dir;
		my $vcf_gene_file = $vcf_output_dir.$gene_info{$gene}{'system_num'}."_".$gene_info{$gene}{'system_symbol'}."-".$gene.".vcf";

		my $cmd = "python $app_client_code_dir/phase.py --typing_data_dir='$root_dir_data' --phasing_data_filepath='$phasing_data_file' --vcf_gene_file='$vcf_gene_file' --accession=$accession --genomic_chr=$chr --genomic_start_pos=$genomic_start_pos --genomic_end_pos=$genomic_end_pos --gene_direction=$gene_info{$gene}{'gene_direction'}";
		print "cmd: ".$cmd."\n";
		system($cmd);
		#exit();
	}	
}

# Make .fa sequence file ...
foreach my $gene (@genes_to_use_list) {
	my $system_sub_dir = "";
	if ($gene_info{$gene}{'system_type'} eq "RBC") {
		$system_sub_dir = "RBC_antigens/";
	} elsif ($gene_info{$gene}{'system_type'} eq "PLT") {
		$system_sub_dir = "PLT_antigens/";
	} elsif ($gene_info{$gene}{'system_type'} eq "HNA") {
			$system_sub_dir = "HNA_antigens/";
	}
	my $seq_output_dir = $gene_seq_dir.$system_sub_dir;
	&make_dir($seq_output_dir, "if_does_not_exist");
		
	# write out the seq ...
	my $fa_file = $seq_output_dir.$gene_info{$gene}{'system_num'}."_".$gene_info{$gene}{'system_symbol'}."-".$gene.".fa";
	print "Making: ".$fa_file."\n";
	open (OUT, ">$fa_file");
	my @gene_seq_array = ();
	my $system_gene_start_by_genomic_pos = $gene_info{$gene}{'system_gene_start_by_genomic_pos'};
	my $system_gene_end_by_genomic_pos = $gene_info{$gene}{'system_gene_end_by_genomic_pos'};
	my $chr = $gene_info{$gene}{'system_chr'};
	$chr =~ s/chr//g;	
	if ($gene_info{$gene}{'gene_direction'} eq "f") {
		for (my $pos = $system_gene_start_by_genomic_pos; $pos <= $system_gene_end_by_genomic_pos; $pos = $pos + 1) {
		    if ($seq{$chr.":".$pos} eq "") {
			    $seq{$chr.":".$pos} = "?";
		    }
			my $gene_pos = $gene_pos_via_genomic_pos{$chr.":".$pos};
		    $gene_seq_array[$gene_pos] = $seq{$chr.":".$pos};
		    #print "seq{$chr:$pos}: ".$seq{$chr.":".$pos}."\n";
		}
	} elsif ($gene_info{$gene}{'gene_direction'} eq "r") {
		for (my $pos = $system_gene_start_by_genomic_pos; $pos >= $system_gene_end_by_genomic_pos; $pos = $pos - 1) {
		    if ($seq{$chr.":".$pos} eq "") {
			    $seq{$chr.":".$pos} = "?";
		    }
			my $gene_pos = $gene_pos_via_genomic_pos{$chr.":".$pos};
			if ($gene eq "ABO") {
				$gene_pos = $patched_gene_pos_via_genomic_pos{$chr.":".$pos};
			}
		    $gene_seq_array[$gene_pos] = $seq{$chr.":".$pos};
		    if ($seq{$chr.":".$pos} ne "?") {
			    #print "seq{$chr:$pos}: ".$seq{$chr.":".$pos}."\n";
			}
		    #print "gene_seq_array[$gene_pos]: ".$gene_seq_array[$gene_pos]."\n";
		}
	}

	my $gene_seq = ">$gene\n";
	for my $for (1 .. $#gene_seq_array) {
		if ($gene_seq_array[$for] eq "") {
			$gene_seq_array[$for] = "?";
		}
		# override for ABO del261 issue, since we take care of gene position 17694 already above ...
		if ($gene eq "ABO" && $for eq "17694") {
			$gene_seq_array[$for] = "";	
		}
		if ($gene_seq_array[$for] ne "?") {
			#print "gene_seq_array[$for]: ".$gene_seq_array[$for]."\n";
		}
		#print "gene_seq_array[$for]: ".$gene_seq_array[$for]."\n";
		$gene_seq = $gene_seq.$gene_seq_array[$for];
	}
	print OUT $gene_seq;
	close(OUT);
}

# Read in base stats ...
my %base_stats_per_pos_hash = ();
my $raw_reads_file = $root_dir_data.$accession."/raw_data/".$accession."_genotype_raw_reads.txt";
#my $raw_reads_file = $working_dir."ngs/".$accession."_genotype_raw_reads.txt";
if (-e $raw_reads_file) {
	open (IN, $raw_reads_file);
	my $line = <IN>;
	chomp($line);
	my @header = split(/\t/, $line);
	while (<IN>) {
		my $line = $_;
		chomp($line);
		if (!($line =~ /^#/)) {
			my @fields = split(/\t/, $line);
			$fields[0] =~ s/^chr//gi;	# just in case the inpit has chr we will remove it to be consistent
			for my $header_for (0 .. $#header) {
				my $chr_pos = $fields[0].":".$fields[1];
				$base_stats_per_pos_hash{$chr_pos}{$header[$header_for]} = $fields[$header_for];
				#print "base_stats_per_pos_hash{$chr_pos}{$header[$header_for]}: ".$base_stats_per_pos_hash{$chr_pos}{$header[$header_for]}."\n";
			}
		}
	}
}

# Make gene base stats file ...
my %gene_coverage_data = ();
foreach my $gene (@genes_to_use_list) {
	my $system_sub_dir = "";
	if ($gene_info{$gene}{'system_type'} eq "RBC") {
		$system_sub_dir = "RBC_antigens/";
	} elsif ($gene_info{$gene}{'system_type'} eq "PLT") {
		$system_sub_dir = "PLT_antigens/";
	} elsif ($gene_info{$gene}{'system_type'} eq "HNA") {
			$system_sub_dir = "HNA_antigens/";
	}
	my $seq_output_dir = $gene_base_stats_dir.$system_sub_dir;
	&make_dir($seq_output_dir, "if_does_not_exist");
		
	# write out the seq ...
	my $base_stats_file = $seq_output_dir.$gene_info{$gene}{'system_num'}."_".$gene_info{$gene}{'system_symbol'}."-".$gene."_base_stats.txt";
	print "Making: ".$base_stats_file."\n";
	open (OUT, ">$base_stats_file");
	my @line_out_array = ();
	my $system_gene_start_by_genomic_pos = $gene_info{$gene}{'system_gene_start_by_genomic_pos'};
	my $system_gene_end_by_genomic_pos = $gene_info{$gene}{'system_gene_end_by_genomic_pos'};
	my $chr = $gene_info{$gene}{'system_chr'};
	$chr =~ s/chr//g;
	my @headers = ("gene_pos", "A", "G", "C", "T", "del", "ins", "inserted", "ambiguous", "total");
	if ($gene_info{$gene}{'gene_direction'} eq "f") {
		for (my $pos = $system_gene_start_by_genomic_pos; $pos <= $system_gene_end_by_genomic_pos; $pos = $pos + 1) {
			my $gene_pos = $gene_pos_via_genomic_pos{$chr.":".$pos};
			if ($gene eq "ABO") {
				$gene_pos = $patched_gene_pos_via_genomic_pos{$chr.":".$pos};
			}
			$base_stats_per_pos_hash{$chr.":".$pos}{'gene_pos'} = $gene_pos;
			my @fields_for_line = ();
			if ($base_stats_per_pos_hash{$chr.":".$pos}{'A'} ne "") {	# make sure we have infor for this line otherwise lets not add it ..
				for my $header_for (0 .. $#headers) {
					push(@fields_for_line, $base_stats_per_pos_hash{$chr.":".$pos}{$headers[$header_for]});
				}
				push(@line_out_array, join(",", @fields_for_line));			
			}
			$gene_coverage_data{$gene}{'pos_total'}++;
			$gene_coverage_data{$gene}{'coverage_sum'} = $gene_coverage_data{$gene}{'coverage_sum'} + $base_stats_per_pos_hash{$chr.":".$pos}{'A'} + $base_stats_per_pos_hash{$chr.":".$pos}{'G'} + $base_stats_per_pos_hash{$chr.":".$pos}{'C'} + $base_stats_per_pos_hash{$chr.":".$pos}{'T'} + $base_stats_per_pos_hash{$chr.":".$pos}{'del'} + $base_stats_per_pos_hash{$chr.":".$pos}{'ins'} + $base_stats_per_pos_hash{$chr.":".$pos}{'inserted'};
		}
	} elsif ($gene_info{$gene}{'gene_direction'} eq "r") {
		for (my $pos = $system_gene_start_by_genomic_pos; $pos >= $system_gene_end_by_genomic_pos; $pos = $pos - 1) {
			my $gene_pos = $gene_pos_via_genomic_pos{$chr.":".$pos};
			if ($gene eq "ABO") {
				$gene_pos = $patched_gene_pos_via_genomic_pos{$chr.":".$pos};
			}
			$base_stats_per_pos_hash{$chr.":".$pos}{'gene_pos'} = $gene_pos;
			my @fields_for_line = ();
			if ($base_stats_per_pos_hash{$chr.":".$pos}{'A'} ne "") {	# make sure we have infor for this line otherwise lets not add it ..
				for my $header_for (0 .. $#headers) {
					push(@fields_for_line, $base_stats_per_pos_hash{$chr.":".$pos}{$headers[$header_for]});
				}
				push(@line_out_array, join(",", @fields_for_line));			
			}
			$gene_coverage_data{$gene}{'pos_total'}++;
			$gene_coverage_data{$gene}{'coverage_sum'} = $gene_coverage_data{$gene}{'coverage_sum'} + $base_stats_per_pos_hash{$chr.":".$pos}{'A'} + $base_stats_per_pos_hash{$chr.":".$pos}{'G'} + $base_stats_per_pos_hash{$chr.":".$pos}{'C'} + $base_stats_per_pos_hash{$chr.":".$pos}{'T'} + $base_stats_per_pos_hash{$chr.":".$pos}{'del'} + $base_stats_per_pos_hash{$chr.":".$pos}{'ins'} + $base_stats_per_pos_hash{$chr.":".$pos}{'inserted'};
		}
	}

	print OUT join("\n", @line_out_array);
	close(OUT);
}

# get gene average coverage ...
my @system_types = ();
my %data_to_write_gene_coverage_avg = ();
foreach my $gene (@genes_to_use_list) {
	my $average = 0;
	if ($gene_coverage_data{$gene}{'pos_total'} > 0) {
		$average = &round($gene_coverage_data{$gene}{'coverage_sum'} / $gene_coverage_data{$gene}{'pos_total'}, 0);
	}
		
	push(@{$data_to_write_gene_coverage_avg{$gene_info{$gene}{'system_type'}}{'header'}}, $gene);
	push(@{$data_to_write_gene_coverage_avg{$gene_info{$gene}{'system_type'}}{'average'}}, $average);
	
	if (!(grep (/^\Q$gene_info{$gene}{'system_type'}\E$/, @system_types))) {
		push(@system_types, $gene_info{$gene}{'system_type'});
	}
}


##### Functions ...

sub flip_base {
	my $input = shift;
	# reverse strand ...
	my $flipped = scalar reverse $input;
	
	my %replace = (
		'A' => 'T',
		'T' => 'A',
		'C' => 'G',
		'G' => 'C',	
		'a' => 't',
		't' => 'a',
		'c' => 'g',
		'g' => 'c',
	);
	
	my $regex = join "|", keys %replace;
	$regex = qr/$regex/;
	$flipped =~ s/($regex)/$replace{$1}/g;
	
	return  $flipped;
}

##### a.pl FUNCTIONS ....

sub commify {
   my $input = shift;
   $input = reverse $input;
   $input =~ s/(\d\d\d)(?=\d)(?!\d*\.)/$1,/g;
   return reverse $input;
}

sub round {
	my ($number,$decimals) = @_;

    return sprintf("%.".$decimals."f", $number)
}

sub make_dir () {
    my ($dir_to_make, $make_condition) = @_;
    
    if ($make_condition eq "if_does_not_exist") {
        if (!(-e $dir_to_make)) {
        	mkdir ($dir_to_make);
        }
    }
}

