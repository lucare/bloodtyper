#!/usr/bin/perl
#!C:/Perl/bin/perl.exe

#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################

use Fcntl qw(:DEFAULT :flock);
use strict;
#use LWP::UserAgent;
use LWP::UserAgent::Determined;
use HTTP::Request::Common qw(POST);
use Statistics::Basic qw(:all nofill);
use File::Copy;
use Getopt::Long;
use List::Util qw(sum);
use Data::Dumper;
use Digest::MD5 qw(md5_hex);

use FindBin;
use lib $FindBin::Bin.'/my_perl_modules';
use My::wgs_pred_common;

my $accession_cleaned_cmdline = "";
my $root_data_dir_cmd_line = "";
my $root_results_dir_cmd_line = "";
my $run_timestamp = "";
my $D_cnv_to_use_forced = "";
my $C_cnv_to_use_forced = "";
my $reference_GRCh_genome_build_num = "";
my $cmdline_result = GetOptions 	(
							"accession_cleaned=s" => \$accession_cleaned_cmdline,
							"root_data_dir=s" => \$root_data_dir_cmd_line,
							"root_results_dir=s" => \$root_results_dir_cmd_line,
							"sequence_source=s" => \$sequence_source,
							"instrument_type=s" => \$instrument_type,
							"sequence_type=s" => \$sequence_type,
							"probes_to_skip_filename=s" => \$probes_to_skip_filename,
							"probes_to_force_call_filename=s" => \$probes_to_force_call_filename,
							"run_timestamp=s" => \$run_timestamp,
							"D_cnv_to_use_forced=s" => \$D_cnv_to_use_forced,
							"C_cnv_to_use_forced=s" => \$C_cnv_to_use_forced,
							"reference_GRCh_genome_build_num=s" => \$reference_GRCh_genome_build_num,
							"send_sample_name_to_typing_service=s" => \$send_sample_name_to_typing_service,
							"website_version_override=s" => \$website_version_override,
							"demo=s" => \$demo,
							"access_code=s" => \$access_code,
							);
print "Sequence source: ".$sequence_source."\n";
print "Instrument type: ".$instrument_type."\n";
print "Sequence type: ".$sequence_type."\n";
print "D_cnv_to_use_forced: ".$D_cnv_to_use_forced."\n";
print "C_cnv_to_use_forced: ".$C_cnv_to_use_forced."\n";
print "run_timestamp: ".$run_timestamp."\n";
print "root_data_dir_cmd_line: ".$root_data_dir_cmd_line."\n";
print "root_results_dir_cmd_line: ".$root_results_dir_cmd_line."\n";
print "reference_GRCh_genome_build_num: ".$reference_GRCh_genome_build_num."\n";
print "send_sample_name_to_typing_service: ".$send_sample_name_to_typing_service."\n";
print "website_version_override: ".$website_version_override."\n";
print "demo: ".$demo."\n";
print "access_code: ".$access_code."\n";

&get_accession_to_use($accession_cleaned_cmdline, $root_data_dir_cmd_line, $root_results_dir_cmd_line);
&get_api_key();

my @accessions_to_process_list = ();
my $coverage_result_html = "";

push (@accessions_to_process_list, $accession);


#my $working_dir = "";
my $wgs_dir = "";

my $pred_output_dir_rbc = "";


foreach my $accession (@accessions_to_process_list) {
	$wgs_dir = $root_dir_data.$accession."/raw_data/";
	my $pred_output_dir_root = $root_dir_results.$accession."/";
	&make_dir($pred_output_dir_root, "if_does_not_exist");
	$pred_output_dir_rbc = $pred_output_dir_root."RBC_antigens/";
	&make_dir($pred_output_dir_rbc, "if_does_not_exist");
	if ($sequence_type eq "MIP") {
		$sequence_type = "WES";
	}
	if ($sequence_type eq "WGS" || $sequence_type eq "WES") {
		&create_coverage_file_new($accession);
	} elsif ($sequence_type eq "SNP") {
		&SNP_probe_force_calls($accession);
	}
}

exit();

sub SNP_probe_force_calls () {
	my ($accession) = @_;
	my @before_prediction_allele_ids_found = ();
	
	my $working_dir_data = $root_dir_data.$accession."/";

	# read in the SNP probe forced call file and try to find the probes ...
	if (-e $probes_to_force_call_filename) {
		print "Looking for SNP probes to force call ...\n";
		#AX-83058972	0/0:38,N+|1/1:37,M+|0/1:38,N+;37,M+
		open (IN, $probes_to_force_call_filename);
		while (<IN>) {
			my $line = $_;
			chomp($line);
			my @line_fields = split(/\t/, $line);
			my $vcf_filename = $working_dir_data."gene_vcf/RBC_antigens/".@line_fields[0];
			my $probename = @line_fields[1];
			my @call_interps = split(/\|/, @line_fields[2]);
			print "Trying to force call probe: ".$probename."\n";
			my @gt_array = ();
			my %antigen_to_force_call_via_gt_call = ();
			# get the details on how to force call from file ...
			foreach my $call_interp (@call_interps) {
				my @interp_split = split(/:/, $call_interp);
				my $gt_call = @interp_split[0];
				my $antigen_to_force_call = @interp_split[1];
				$antigen_to_force_call =~ s/,/\t/gi;
				push(@gt_array, $gt_call);
				$antigen_to_force_call_via_gt_call{$gt_call} = $antigen_to_force_call;
			}
			# look for probe in vcf file and force call ...
			if (-e $vcf_filename) {
				print "Looking for probe signal in: ".$vcf_filename."\n";
				open(VCF_IN, $vcf_filename);
				while (<VCF_IN>) {
					my $line_vcf = $_;
					chomp($line_vcf);
					if ($line_vcf =~ /$probename/) { # we found the line with the probe ...
						my @fields_vcf = split(/\t/, $line_vcf);
						my $gt_call = @fields_vcf[9];
						if ($antigen_to_force_call_via_gt_call{$gt_call} ne "") {
							my @forced_calls = split(/;/, $antigen_to_force_call_via_gt_call{$gt_call});
							foreach my $forced_call (@forced_calls) {
								my $to_push = $forced_call;
								print "Found before typing allele: ".$to_push."\n";
								push(@before_prediction_allele_ids_found, $to_push);
							}
						}
					}
				}
				close(VCF_IN);
			}
		}
		close(IN);		
	}
	
	#print "before_prediction_allele_ids_found: ".Dumper(@before_prediction_allele_ids_found)."\n";
	
	# Write out the before prediction allele ids found ...
	&create_pre_alleles_found_file($accession, \@before_prediction_allele_ids_found);
}


sub create_coverage_file_new() {
	my ($accession) = @_;
	
	my $copy_number_analysis_form_input = "";
	
	# read in copy_number_regions definitions ...
	my $copy_number_regions_filename = $pipeline_supporting_files_dir."copy_number_regions.txt";
	open (IN, $copy_number_regions_filename);
	my %cnv_regions = ();
	my @region_names = ();
	my @basestat_chrpos_to_get = ();
	my %basestat_region_names_via_chrpos = ();
	my %basestat_base_to_get_via_region_name = ();
	my %area_name_via_chrpos = ();
	while (<IN>) {
		my $line = $_;
		chomp($line);
		if ($line =~ /^basestat/) {	# this is a single base to get full base stats for ...
			#basestat	M_59	4	145041707	A
			my @fields = split(/\t/, $line);
			my $area_name = $fields[0];
			my $region_name = $fields[1];
			my $chrpos = $fields[2].":".$fields[3];
			push(@basestat_chrpos_to_get, $chrpos);
			push(@{$basestat_region_names_via_chrpos{$chrpos}}, $region_name);
			$area_name_via_chrpos{$chrpos} = $area_name;
			$basestat_base_to_get_via_region_name{$region_name} = $fields[4];
		} else {	# this is a region to get coverage data ...
			#RHD-upstream	1	25580018	25593111
			my @fields = split(/\t/, $line);
			my $region_name = $fields[1];
			push(@region_names, $region_name);
			$cnv_regions{$region_name}{'area_name'} = $fields[0];
			$cnv_regions{$region_name}{'region_name'} = $fields[1];
			$cnv_regions{$region_name}{'chr'} = $fields[2];
			$cnv_regions{$region_name}{'genomic_start'} = $fields[3];
			$cnv_regions{$region_name}{'genomic_end'} = $fields[4];
		}
	}
	close(IN);

	# read in raw base stats ...
	my $file_in  = $wgs_dir.$accession."_genotype_raw_reads.txt";
	my %coverage_via_chrpos = ();
	open(IN, $file_in);
	while (<IN>) {
		#chr	bp	A	G	C	T	del	ins	inserted	ambiguous
		#1	3689034	0	0	18	0	0	0	.	.
		my $line = $_;
		chomp($line);
		my @fields = split(/\t/, $line);
		my $chrpos = $fields[0].":".$fields[1];
		my %base_stats_nums = ();
		$base_stats_nums{'A'} = $fields[2];
		$base_stats_nums{'G'} = $fields[3];
		$base_stats_nums{'C'} = $fields[4];
		$base_stats_nums{'T'} = $fields[5];
		$base_stats_nums{'del'} = $fields[6];
		$base_stats_nums{'ins'} = $fields[7];
		$base_stats_nums{'inserted'} = $fields[8];
		$base_stats_nums{'ambiguous'} = $fields[9];
		# Get the total num of base calls at this position ...
		my @bases = ("A", "G", "C", "T", "del", "ins", "inserted", "ambiguous");
		for my $base (@bases) {
			$base_stats_nums{'total'} = $base_stats_nums{'total'} + $base_stats_nums{$base};
		}
		$coverage_via_chrpos{$chrpos} = $base_stats_nums{'total'};
		#print "$coverage_via_chrpos{$chrpos}: ".$coverage_via_chrpos{$chrpos}."\n";
		if (grep (/^\Q$chrpos\E$/, @basestat_chrpos_to_get)) {
			#print "Found basestat line we need: $chrpos\n";
			# Go through the region_name for this basestat chrpos ...
			foreach my $region_name (@{$basestat_region_names_via_chrpos{$chrpos}}) {
				$copy_number_analysis_form_input = $copy_number_analysis_form_input.$area_name_via_chrpos{$chrpos}."\t".$region_name."\t".$base_stats_nums{$basestat_base_to_get_via_region_name{$region_name}}."\t".$base_stats_nums{'total'}."\n";
			}
		}
	}
	close(IN);
	
# 	# read in the coverage values ...
# 	my $file_in  = $wgs_dir.$accession."_RH_MNS_BEDtoolscoverage.txt";
# 	open(IN, $file_in);
# 	my %coverage_via_chrpos = ();
# 	while (<IN>) {
# 		my $line = $_;
# 		chomp($line); 
# 		#1	25688739	25747662	RHCE	1	45
# 		my @fields = split(/\t/, $line);
# 		my $chr = $fields[0];
# 		$chr =~ s/^chr//gi;	# just in case the inpit has chr we will remove it to be consistent
# 		my $pos = $fields[1] + $fields[4];
# 		my $coverage = $fields[5];
# 		$coverage_via_chrpos{$chr.":".$pos} = $coverage;
# 		#print "coverage_via_chrpos $chr $pos: ".$coverage_via_chrpos{$chr.":".$pos}."\n";
# 	}
# 	close(IN);
	
	# get the coverage means for each region ...
	foreach my $region_name (@region_names) {
		my $chr = $cnv_regions{$region_name}{'chr'};
		my @coverage_to_combine = ();
		for my $pos ($cnv_regions{$region_name}{'genomic_start'} .. $cnv_regions{$region_name}{'genomic_end'}) {
			my $chrpos = $chr.":".$pos;
			push(@coverage_to_combine, $coverage_via_chrpos{$chrpos});
		}
		$copy_number_analysis_form_input = $copy_number_analysis_form_input.$cnv_regions{$region_name}{'area_name'}."\t".$cnv_regions{$region_name}{'region_name'}."\t".sum(@coverage_to_combine)."\t".eval($#coverage_to_combine+1)."\n";
	}
	
	# send data to app server and perform copy number analysis ...
	print "copy_number_analysis_form_input: ".$copy_number_analysis_form_input."\n";
	my $copy_number_data_input_file_out  = $pred_output_dir_rbc.$accession."_copy_number_data_input.txt";
	print "Creating copy_number_data_input file: ".$copy_number_data_input_file_out."\n";
	open(OUT, ">$copy_number_data_input_file_out");
	print OUT $copy_number_analysis_form_input;
	close(OUT);

	my $url_content = &call_copy_number_webservice(0, $copy_number_analysis_form_input);
	#print "url_content: ".$url_content."\n";

	# get app server copy number results and save to files ...
	my @file_contents = split(/~BREAK~\n/, $url_content);
	my $cnv_file_out  = $pred_output_dir_rbc.$accession."_copy_number_analysis.txt";
	print "Creating CNV data file: ".$cnv_file_out."\n";
	open(OUT, ">$cnv_file_out");
	print OUT $file_contents[0];
	close(OUT);
	#
	my $pre_prediction_file_out  = $pred_output_dir_rbc.$accession."_pre_typing_allele_ids_found.txt";
	print "Creating pre typing allele ids file: ".$pre_prediction_file_out."\n";
	open(OUT, ">$pre_prediction_file_out");
	print OUT $file_contents[1];
	close(OUT);
	#
	my $pre_prediction_file_out  = $pred_output_dir_rbc.$accession."_pre_typing_structural_gene_changes_found.txt";
	print "Creating pre structural_gene_changes file: ".$pre_prediction_file_out."\n";
	open(OUT, ">$pre_prediction_file_out");
	print OUT $file_contents[2];
	close(OUT);
	#
	my $svg_file_out  = $pred_output_dir_rbc.$accession."_copy_number_plots.svg";
	print "Creating CNV Plots: ".$svg_file_out."\n";
	open(OUT, ">$svg_file_out");
	print OUT $file_contents[3];
	close(OUT);
}

sub call_copy_number_webservice($try_num) {
	my ($try_num, $copy_number_analysis_form_input) = @_;
	
	$try_num++;
	if ($try_num > 10) {
		print "ERROR: Giving up after 10 tries to call webservice\n";
		exit();
	}
	print "Calling copy number webservice at ".$app_server.$app_script.". Try #".$try_num."\n";
	my $sleep_amount = 60*($try_num-1);
	if ($sleep_amount > 0) {
		print "Sleeping for: ".$sleep_amount." seconds"."\n";
		sleep $sleep_amount;
	}
	
	#open("OUT", ">>md5_cnv.txt");
	#print OUT $accession."\t".""."\t".md5_hex($copy_number_analysis_form_input)."\n";
	#close(OUT);
	
	my $ua = LWP::UserAgent::Determined->new;
    $ua->timeout(300);
    $ua->timing("10,30,90");
    $ua->env_proxy;
    my $url_content = "";
    my $request = POST $app_server.$app_script, [
    					api_key => $api_key,
    					sample_name => $sample_name,
    					access_code => $access_code,
    					todo => 'copy_number_analysis_form_process',
    					sequence_type => $sequence_type,
    					coverage_data => $copy_number_analysis_form_input,
    					D_cnv_to_use => $D_cnv_to_use_forced,
    					C_cnv_to_use => $C_cnv_to_use_forced,
    					run_timestamp => $run_timestamp
    					];
    my $response = $ua->request($request);
    if ($response->is_success) {
        $url_content = $response->content;
        return $url_content;
    } else {
	    print "WARNING ".$response->status_line."\n";
	    print "Will try again ...\n";
        $url_content = &call_copy_number_webservice($try_num, $copy_number_analysis_form_input);
        return $url_content;
    }
}

sub create_pre_alleles_found_file () {
	my ($accession, $before_prediction_allele_ids_found_ref) = @_;
	# Write out the before prediction allele ids found ...
	my $before_prediction_allele_ids_found_file = $pred_output_dir_rbc.$accession."_pre_typing_allele_ids_found.txt";
	print "Saving Before Typing Alleles Found to: ".$before_prediction_allele_ids_found_file."\n";
	#print Dumper($before_prediction_allele_ids_found_ref);
	open(OUT, ">$before_prediction_allele_ids_found_file");
	print OUT join("\n", @{$before_prediction_allele_ids_found_ref});
	close(OUT);
}

sub round {
	my ($number,$decimals) = @_;

    return sprintf("%.".$decimals."f", $number)
}

sub make_dir () {
    my ($dir_to_make, $make_condition) = @_;
    
    if ($make_condition eq "if_does_not_exist") {
        if (!(-e $dir_to_make)) {
        	mkdir ($dir_to_make);
        }
    }
}
