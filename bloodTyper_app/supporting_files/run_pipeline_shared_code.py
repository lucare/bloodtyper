#========================== run_pipeline_shared_code.py ==========================#
#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################

import sys
import os
import time
import datetime
import urllib2
import re


#---------------------------------------------------------------------
# Run Timestamp ...
#---------------------------------------------------------------------
def get_run_timestamp(run_timestamp):
	if (run_timestamp == '' or run_timestamp == None):
		ts = time.time()
		run_timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
	run_timestamp_for_filesnames = run_timestamp
	run_timestamp_for_filesnames = run_timestamp_for_filesnames.replace(" ", "_")
	run_timestamp_for_filesnames = run_timestamp_for_filesnames.replace(":", "-")

	return run_timestamp, run_timestamp_for_filesnames

#---------------------------------------------------------------------
# Read in the app config values ...
#---------------------------------------------------------------------
def get_app_config_values():
	app_config = {}
	app_config_filename = 'app_config.txt'
	print 'Reading in app config from: ' + app_config_filename
	file_in = open(app_config_filename,'r')
	for line in file_in:
		line = line.strip()
		if not line.startswith("#"):
			fields = line.strip().split('\t')
			if len(fields) < 2:	# value was left blank
				fields.append('')
			value = fields[1]
			app_config[fields[0]] = value
			print fields[0] + " ==> " + value
		
	
	return app_config

#---------------------------------------------------------------------
# Get app/api key ...
#---------------------------------------------------------------------
def get_app_key():
	app_key = ''
	app_key_filename = 'app_key.txt'
	if os.path.isfile(app_key_filename):
		file_in = open(app_key_filename,'r')
		for line in file_in:
			line = line.strip()
			if not line.startswith("#"):
				fields = line.strip().split('\t')
				if len(fields) < 2:	# value was left blank
					fields.append('')
				if fields[0] == 'app_key':
					app_key = fields[1]
		file_in.close()
	if app_key == '':
		print 'app_key NOT found.'
		response = raw_input('Please enter your app_key: ')
		fileout = open(app_key_filename, 'w')
		fileout.write('app_key\t'+response)
		fileout.close()
		print 'Saved app_key: '+response
		print 'Please re-run pipeline'
		exit()
		
	return app_key

#---------------------------------------------------------------------
# Read in the project config values ...
#---------------------------------------------------------------------
def get_project_config_values(app_config, project, batch):
	project_config = {}
	if (project == ""):
		project = app_config['app_default_project']
	project_config_dir = app_config['project_configs_dir']+project+'/'
	project_config['project_config_filepath_prefix'] = project_config_dir+project.replace('/', '_')
	project_config_filename = project_config['project_config_filepath_prefix']+'--project_config.txt'
	print 'Reading in project config from: ' + project_config_filename
	file_in = open(project_config_filename,'r')
	for line in file_in:
		line = line.strip()
		if not line.startswith("#"):
			fields = line.strip().split('\t')
			if len(fields) < 2:	# value was left blank
				fields.append('')
			value = fields[1]
			value = value.replace('{APP_DIR}', app_config['app_client_code_dir'])
			value = value.replace('{BATCH}', batch)
			project_config[fields[0]] = value
			#print fields[0] + " ==> " + value
	file_in.close()
	# set genome reference ...
	project_config['reference_GRCh_genome_build_num'] = project_config.get('reference_GRCh_genome_build_num','37')
	# Add value for Congif values that reference other Config values ...
	for key, value in project_config.iteritems():
		for key2, value2 in project_config.iteritems():
			tosearch = '{CONFIG[\''+key2+'\']}'
			if tosearch in project_config[key]:
				project_config[key] = value.replace(tosearch, value2).replace('//', '/')
		print key + ' ==> ' + project_config[key]
		
	if project_config.get('website_version_override', ''):
		app_config['app_script'] = 'cgi-bin/a_v'+project_config.get('website_version_override', '')+'/a.fpl'
		
	return project_config

#---------------------------------------------------------------------
# Get server version and check against client version ...
#---------------------------------------------------------------------
def get_and_check_versions(app_config, app_key):
	url = app_config['app_server']+app_config['app_script']+'?todo=get_versions&api_key='+app_key
	print 'Checking for typing client updates at '+url+" ..." 
	#print "url: "+url
	page_contents = urllib2.urlopen(url).read()
	#print "page_contents: "+page_contents
	searchObj = re.search( r'<!--typing_client_pipeline_version_START-->(.*)<!--typing_client_pipeline_version_END-->', page_contents, re.M|re.I)
	if searchObj:
		server_expecting_version = float(searchObj.group(1))
		client_version = float(app_config['pipeline_version'])
		print "server_expecting_version: " + str(server_expecting_version)
		print "pipeline client version: " + str(client_version)
	if server_expecting_version != client_version:
		print "Server and client versions different. Please update the client to version " + str(server_expecting_version)
		exit()