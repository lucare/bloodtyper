import sys
import re

# From http://blog.nextgenetics.net/?e=56
# With help from http://seqanswers.com/forums/showthread.php?t=25483
# Modified by William Lane <WLANE@PARTNERS.ORG> to better parse insertions and deletions using regrex

#========================= mpileup_to_bases_calls.py ========================#
#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################

class Matcher(object):
    def __init__(self):
        self.matches = None
    def set(self, matches):
        self.matches = matches
    def __getattr__(self, name):
        return getattr(self.matches, name)

class re2(object):
    def __init__(self, expr):
        self.re = re.compile(expr)

    def match(self, matcher, s):
        matches = self.re.match(s)
        matcher.set(matches)
        return matches

inFile = open(sys.argv[1],'r')

print 'chr\tbp\tA\tG\tC\tT\tdel\tins\tinserted\tambiguous'
for line in inFile:
	data = line.strip().split('\t')
	chr = data[0]
	bp = data[1]
	bases = data[4].upper()
	ref = data[2].upper()
	
	types = {'A':0,'G':0,'C':0,'T':0,'-':0,'+':0,'X':[]}
	
	#print('')
	#print('line: '+line)
	#print('ref: '+ref)
	#print('bases: '+bases)
	#if (bp == '3689216'):
	#	print('here')
	#	print('bases: '+bases)
	#	exit()
	
	i = 0
	base_list = re.compile("(\.|\*|,|\$|\^\D|C|G|T|A|[-|+]\d*[T|G|A|C|t|g|a|c]*)").split(bases)
	#base_list = re.compile("(\.|,|\$|\^\D)").split(bases)
	
	del_pattern = re2("^-(.*)")
	del_m = Matcher()
	ins_pattern = re2("^\+(.*)")
	ins_m = Matcher()
	hat_pattern = re2("^\^")
	hat_m = Matcher()

	while i < len(base_list):
		base = base_list[i]
		#print(base)
		if base == '':
			''
		elif hat_pattern.match(hat_m, base):
			i += 1
		elif base == '$':
			i += 1
		elif del_pattern.match(del_m, base):
			i += 1
			#addNum = int(bases[i])
			#addSeq = ''
			#for a in range(addNum):
			#	i += 1
			#	addSeq += bases[i]
			
			types['-'] += 1
		elif ins_pattern.match(ins_m, base):
			#i += 1
			#addNum = int(bases[i])
			#addSeq = ''
			#for a in range(addNum):
			#	i += 1
			#	addSeq += bases[i]
			
			types['+'] += 1
		elif base == '*':
			types['-'] += 1
		elif base == '.' or base == ',':
			types[ref] += 1
		else:
			if types.has_key(base):
				types[base] += 1
			else:
				types['X'].append(base)
		#if base != '':
			#print('base: '+base)
		
		i += 1
	
	adds = '.'
	#if len(types['+']) > 0:
	#	adds = ','.join(types['+'])
	
	amb = '.'
	if len(types['X']) > 0:
		amb = ','.join(types['X'])
		
	#total = types['A']+types['G']+types['C']+types['T']+types['-']+len(types['+'])
	
	out = [chr,bp,types['A'],types['G'],types['C'],types['T'],types['-'],types['+'],adds,amb]
	print '\t'.join([str(x) for x in out])