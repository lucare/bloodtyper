#!/usr/bin/perl
#!C:/Perl/bin/perl.exe

#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################

use Fcntl qw(:DEFAULT :flock);
use strict;
use DBI;
use Getopt::Long;

use FindBin;
use lib $FindBin::Bin.'/my_perl_modules';
use My::wgs_pred_common;

my $tmp_dir = "";
my $gatk_path = "";
my $ref_fasta_file = "";
my $bam_file_in = "";
my $out_vcf = "";
my $bldloci_list = "";

my $cmdline_result = GetOptions 	(
							"tmp_dir=s" => \$tmp_dir,
							"gatk_path=s" => \$gatk_path,
							"ref_fasta_file=s" => \$ref_fasta_file,
							"bam_file_in=s" => \$bam_file_in,
							"out_vcf=s" => \$out_vcf,
							"bldloci_list=s" => \$bldloci_list,
						);

my $cmd="java -Xmx4g -Djava.io.tmpdir=$tmp_dir -jar $gatk_path -T UnifiedGenotyper  -R $ref_fasta_file -I $bam_file_in -dcov 1000 -glm BOTH -o $out_vcf -out_mode EMIT_ALL_SITES -stand_call_conf 0.5 -L $bldloci_list";

print "cmd: ".$cmd."\n";

system($cmd);