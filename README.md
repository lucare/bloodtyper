![bloodTyper](bloodTyper_app/images/bloodTyper_title_logo.png)

bloodTyper is a software package that can automatically type RBC and PLT antigens from next generation sequencing (NGS) data. It has primarily been validated on whole genome sequencing (WGS) data.

Additional, efforts are underway to expand bloodTyper to whole exome sequencing (WES), targeted NGS, and SNP array data.

- [Getting Started](#markdown-header-getting-started)
    - [Install bloodTyper](#markdown-header-install-bloodtyper)
    - [Install Dependancies](#markdown-header-install-dependancies)
    - [App Key](#markdown-header-app-key)
    - [Download Example NGS Data](#markdown-header-download-example-ngs-data)
    - [Updated Project Configuration File](#markdown-header-update-example-project-configuration-file)
    - [Run bloodTyper](#markdown-header-run_bloodtyper)
    - [View Results](#markdown-header-view-results)
- [Example Datasets](#markdown-header-support)
    - [NA12878 Genome](#markdown-header-na12878-genome)
    - [MedSeq Project Genomes](#markdown-header-medseq-project-genomes)
- [Documentation](#markdown-header-documentation)
    - [bloodTyper Overview](#markdown-header-bloodtyper_overview)
    - [Project Config Files](#markdown-header-project-config-files)
    - [Project Code Files](#markdown-header-project-code-files)
    - [Data Files](#markdown-header-data-files)
- [Run Modes](#markdown-header-run-modes)
    - [Batch Run Mode](#markdown-header-batch-run-mode)
    - [List Run Mode](#markdown-header-list-run-mode)
- [Support](#markdown-header-support)

# Getting Started
***
## Install bloodTyper
- `git clone https://bitbucket.org/lucare/bloodTyper.git`
## Install Dependancies
- [Python 2.7](https://www.python.org/download/releases/2.7/)
- [Perl 5](https://www.perl.org/get.html)
- Additional [Perl CPAN Modules](https://www.cpan.org/)
	- Net::SSLeay
	- IO::Socket::SSL
	- LWP::UserAgent::Determined
	- Statistics::Basic
	- LWP::Protocol::https
	- Compress::LZF
- [GATK](https://software.broadinstitute.org/gatk/)
- [bedtools](http://bedtools.readthedocs.io/)
- [HTSlib](https://github.com/samtools/htslib)
- [samtools](https://github.com/samtools/samtools)
## App Key
- App Key = an alphanumeric key that identifies your installation and is required to run your own samples.
- Keys can be be requested at [https://bloodantigens.com/cgi-bin/a/a.fpl?todo=typing_software_show]
- To add your key just run `python _run_bloodTyper.py` and enter your key.
- Note: You do NOT need an App Key if you are only typing pre-approved example samples using --demo
## Download Example NGS Data
- `git clone https://bitbucket.org/lucare/bloodTyper_example_data.git`
- This will create the data directory `bloodTyper_example_data/` and download a 125 MB example WGS alignment file (.bam) extracted for just the RBC and PLT antigen genes
## Updated Example Project Configuration File
- In a text editor update settings in the example configuration file `bloodTyper/project_files/example/example--project_config.txt`
## Run bloodTyper
- `cd bloodTyper`
- `python _run_bloodTyper.py --project=example --demo`
- `python _run_bloodTyper.py --project=example --demo --just_summary`
## View Results
- View `bloodTyper_example_data/typing_results/index.html` in a web browser to see the results.
- You can see the expected tpying result output at [https://bloodantigens.com/data/bloodTyper_example_data](https://bloodantigens.com/data/bloodTyper_example_data)
- `bloodTyper_example_data/typing_data_files/` = directory containing data files created during the typing. The files in these directories are not needed to share the results.
- `bloodTyper_example_data/typing_results/` = directory contating the results. You can share just this directory by when  distrubuting the results.

# Example Datasets
***
Anyone can freely use bloodTyper to type selected example datasets. If you wish to type your own data you will need to request an [App Key](#markdown-header-app-key).
## NA12878 Genome
Anyone can freely download the NA12878 genome and run it through bloodTyper (see [Getting Started](#markdown-header-getting-started) for step by step directions).
## MedSeq Project Genomes
Upon publication of our paper, you will be able to use bloodTyper on all 110 genomes from the MedSeq Project without an App Key. Before, publication an access code included on the manuscript cover page is required. However, due to IRB restrictions the genomic data files are only avaliable through dbGaP to verified researchers.
### Download Genomes from dbGAP
- Request access to the MedSeq genomes at [dbGaP](https://www.ncbi.nlm.nih.gov/projects/gap/cgi-bin/study.cgi?study_id=phs000958)
- We recommend that you download the smaller BAM files that contain just the RBC and PLT antigen genes (e.g. MEDSEQ001.extract_for_bloodantigens-full_gene_master.bam).
- Create the following directory structure  `medseq_data/full_gene_master_alignments` and download .bam and .bai files to `full_gene_master_alignments`
### Updated MedSeq Project Configuration File
- In a text editor update settings in the example configuration file `bloodTyper/project_files/medseq/medseq--project_config.txt`
- Note: It is possible to run the data against an archived version of the bloodantigens.com database server to exactly match the version used in the original MedSeq analysis publication. To do this uncomment (remove leading '#') the following option in the config file so it becomes `website_version_override    3.4`
- Before, publication an access code included on the manuscript cover page is required, this is set in the config file.
### Run bloodTyper
- `cd bloodTyper`
- Test that things work by typing just the first sample
    - `python _run_bloodTyper.py --project=medseq --demo --sample=MEDSEQ001`
- Then run on all 110 samples
    -`python _run_bloodTyper.py --project=medseq --demo`
- `python _run_bloodTyper.py --project=medseq --demo --just_summary`
### View Results
- View `medseq_data/typing_results/index.html` in a web browser to see the results.

# Documentation
***
## bloodTyper Overview
### Run Options
- `python _run_bloodTyper.py --project=PROJECT_NAME {--batch=BATCH_NAME} {--skip=pre,1,2,3,4,5,6,7,8,post} {--summary} {--just_summary} {--demo}`
### Program Steps
- pre = a customizable script that runs before the pipeline
- 1 = make\_antigen\_gene\_master\_bam
- 2 = make\_exon\_subset\_bam
- 3 = genotype\_antigen\_genes
- 4 = make\_antigen\_gene\_coverage\_file
- 5 = split\_to\_gene\_vcf\_seq\_coverage\_files
- 6 = pre\_typing\_analysis
- 7 = type\_antigens\_from\_alleles
- post = a customizable script that runs before the pipeline
## Project Config Files
Project Config files allow for project specific configuration.
### File / Directory Structure
- Note: Project specific code should only go under the subfolder `project_files/` this will keep the typing app code separate and easier to update.
- `project\_files/PROJECTNAME/{PROJECTSUBNAME}/
- You can comment out specific lines in the config files by starting the line with #
- `PROJECTNAME_{PROJECTSUBNAME}--project_config.txt`
	- Main config file with custom settings specific for the project
	- Folder paths can be specified relative to `_run_bloodTyper.py` script.
	- Always terminate folder paths values with with /
	- You can defined some placeholder variables that will be evaluated a run:
		- `{APP_DIR}` = the absolute path of \_run\_bloodTyper.py
		- `{BATCH}` = the batch name passed to \_run\_bloodTyper.py
		- `{CONFIG['NAME']}` = the value of another setting in the config file.
	- Settings:
		- reference\_GRCh\_genome\_build\_num = 37,38
Replace NAME in above example with the name of the setting value you want to use
- `PROJECTNAME_{PROJECTSUBNAME}--probes_to_force_call.txt`
	- Do not need to create this file if not using. Has a syntax that allows for array probes to force all specific antigens.
	- Format = new line for each probe
	- Example:
		- 034\_VEL-SMIM1.vcf AX-86577342 0/0:306,Vel+|1/1:307,Vel-|0/1:306,Vel+;307,Vel-
- `PROJECTNAME_{PROJECTSUBNAME}--probes_to_skip.txt`
	- You do not need to creat this file if not using. Simply a list of probes to skip (generally bad or poor reacting probes). 
	- Format = new line for each probes
- `PROJECTNAME_{PROJECTSUBNAME}--to_run_genes.txt`
	- If empty then all genes will be run
- `PROJECTNAME_{PROJECTSUBNAME}--to_run_samples.txt`
	- For listing sample names when running in List Run Mode
	- Can leave empty if running in Batch Run Mode
## Project Code Files
Allow for project specific code to run before or after the pipeline
### File / Directory Structure
- Note: Project specific code should only go under the subfolder `project_files/` this will keep the typing app code separate and easier to update.
- `project_files/PROJECTNAME/{PROJECTSUBNAME/}/code/`
- These should be placed either under in a subfolder called `code`. If there is a custom script that would apply to all PROJECTSUBNAMES then put it in 'code' subfolder in `project_files/PROJECTNAME/`
### Configuration
- Set in Project Config File
- `{pre_pipeline_script}` = defines a script to run before the pipeline.
    - For example an script that might read in an array multisample vcf and split it into case specific vcf files and store those then in case folders organized for the pipeline to run.
- `pre_pipeline_script  perl project_files/INTERVAL/ARRAY/code/array_vcf_to_sample_vcf.pl --batch={BATCH} --data_dir={CONFIG['data_dir_root']}`
## Data Files
Starting data comes from BAM or vcf files, including SNP array vcf
### File / Directory Structure
- Set in Project Config File
- Input Folders:
	- `{starting_alignment_dir}` = place where original BAM files are located. These are generally very large and stored in a place outside of the antigen specific data
	- `{data_dir_root}/raw_data/` = place to put Array vcf file
- Output Folders:
	- `{data_dir_root}` = folder under which file extracts and typing results will be placed. For batch runs these are stored typically in separate batch specific subfolders, such as `/data/PROJECT_NAME/{BATCH}`
	- `{data_dir_root}/full_gene_master_alignments_dir` = folder which will contain an antigen gene extract of the BAM. It can be used later to re-type without need for original larger genome wide BAM file.
	- `{typing_data_files_dir_root}` = folder which will contain samples subfolders with the various data files and data used to type each sample
	- `{typing_results_dir_root}` = older which will contain samples subfolders with the typing results

# Run Modes
***
## Batch Run Mode
Example use might be a set of Array data with multiple samples in it. These would be run all together and sample names etc would be automatically determined and all samples run through pipeline.
### Configuration
- Set in Project Config File
- `{sample_mode}` = batch
### Run Example
- `python _run_bloodTyper.py --project=INTERVAL/ARRAY --batch={BATCH}`
### Run Steps
- Will get samples to run automatically using folder names in `{starting_alignment_dir}` or `{typing_results_dir_root}`
- Will then type the antigens
## List Run Mode
Example use might be group of different samples not run or data organized batches. Rather you want to run by giving a list of samples.
### Configuration
- Set in Project Config File
- `{sample_mode}` = list
### Run Example
- `python _run_bloodTyper.py --project=medseq/blind_rerun`
### Run Steps
- Will get samples to run automatically by looking in  `PROJECTNAME_{PROJECTSUBNAME}--to_run_samples.txt`
- Will then type the antigens

# Support
***
- If you have any questions or need help please email William Lane <wlane@bwh.harvard.edu>
