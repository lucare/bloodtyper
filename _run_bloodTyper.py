#========================== _run_antigen_typing.py ==========================#
#=============================== BloodAntigens ==============================#
#  By: William Lane <WLANE@BWH.HARVARD.EDU>
#                    June 2013
#
#  Program Homepage: http://BloodAntigens.com
#==============================================================================#
#
################################################################################
#                               USER LICENSE
# BloodAntigens is a DB and algorithm for DNA typing of RBC & PLT antigens.
# Copyright (C) - 2013  William Lane <WLANE@BWH.HARVARD.EDU>
#                     http://BloodAntigens.com
#-------------------------------------------------------------------------------
# This software can ONLY be used without a license on pre-approved published 
# samples using the --demo option.
#
# Additional use of the software CANNOT is not allowed without a valid license 
# by either commerical or non-commercial entities. 
# 
# To use this software you must obtain such a license
# by contacting the author via email or by visiting the above web site.
#
# The Licensee may use and modify the Software solely for its internal use, as
# long as this license remains intact and that they do not alter or
# remove the footer displayed at the bottom of the generated web pages.
# The Licensee shall not sell, give, disclose, lend, transfer, sublicense or
# otherwise distribute the Software to any third party. Instead, please direct
# others to the above website if they are interested in getting the program.
# It is explicitly forbidden, to sell this software or otherwise make money
# out of it, without approval of the author.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. That is you agree
# not to hold the author responsible for any loss or errors resulting from
# use of this program.
################################################################################

import sys
import os
import argparse
import math
import pipes
import subprocess
import json
import re
import os.path
import glob
import time
import fnmatch
import datetime
sys.path.append('bloodTyper_app/supporting_files')
import run_pipeline_shared_code


#example:  python _run_antigen_typing`.py --skip=a,b --project=medseq

#---------------------------------------------------------------------
# Parse command line arguments ...
#---------------------------------------------------------------------
parser = argparse.ArgumentParser(description='BloodAntigens', epilog="""""",fromfile_prefix_chars='@')
parser.add_argument('-p','--project',dest='project', action='store', default='',
				   help='project')
parser.add_argument('-b','--batch',dest='batch', action='store', default='',
				   help='batch')
parser.add_argument('-anst','--accession_batch_start',dest='accession_batch_start', action='store', default='',
				   help='accession_batch_start')
parser.add_argument('-abs','--accession_batch_size',dest='accession_batch_size', action='store', default='',
				   help='accession_batch_size')
parser.add_argument('-sa','--sample',dest='sample', action='store', default='',
				   help='accession')
parser.add_argument('-s','--skip',dest='skip', action='store',default='',
				   help='skip this step computation')
parser.add_argument('-su','--summary',dest='summary', action='store_true',help='create summary')
parser.add_argument('-jsu','--just_summary',dest='just_summary', action='store_true',help='create just summary')
parser.add_argument('-rts','--run_timestamp',dest='run_timestamp', action='store',help='run_timestamp')
parser.add_argument('-d','--demo',dest='demo', action='store_true',help='demo')
args = parser.parse_args()
accession=args.sample
accession_batch_start=args.accession_batch_start
accession_batch_size=args.accession_batch_size
batch=args.batch
skip=args.skip.split(',')
project = args.project
summary=args.summary
just_summary=args.just_summary
run_timestamp=args.run_timestamp
demo=args.demo
print('accession_batch_start: '+accession_batch_start)
print('accession_batch_size: '+accession_batch_size)


#---------------------------------------------------------------------
# Get run timestamp if needed ...
#---------------------------------------------------------------------
run_timestamp, run_timestamp_for_filesnames = run_pipeline_shared_code.get_run_timestamp(run_timestamp)

#---------------------------------------------------------------------
# Get app and project config values ...
#---------------------------------------------------------------------
app_config = run_pipeline_shared_code.get_app_config_values()
project_config = run_pipeline_shared_code.get_project_config_values(app_config, project, batch)

#---------------------------------------------------------------------
# Check for app key ...
#---------------------------------------------------------------------
if demo == True:
	app_key = "demo"
else:
	app_key = run_pipeline_shared_code.get_app_key()

#---------------------------------------------------------------------
# Check app versions ...
#---------------------------------------------------------------------
run_pipeline_shared_code.get_and_check_versions(app_config, app_key)


# add default skips to list ...
if project_config.get('skip_by_default','') != '':
	for part in project_config['skip_by_default'].split(','):
		skip.append(part.strip())
print 'Skipping Steps: ' + ','.join(skip)

project_config['probes_to_force_call_filename'] = project_config['project_config_filepath_prefix']+'--probes_to_force_call.txt'
project_config['probes_to_skip_filename'] = app_config['project_configs_dir']+project+'--probes_to_skip.txt'
supporting_files_dir = 'supporting_files/'

# run pre pipeline script if there is one ...
if project_config.get('pre_pipeline_script','') != '' and not just_summary:
	print "\nINFO: Step Pre: Pre pipeline script" 
	if 'pre' not in skip:
		print 'Running pre_pipeline_script: '+project_config['pre_pipeline_script'],'\n'
		res=os.system(project_config['pre_pipeline_script'])
		if res != 0: sys.exit(1)
	else:
		print "Skipping this step ..."

# Read in genes to run ...
project_to_run_genes_filename = project_config['project_config_filepath_prefix']+'--to_run_genes.txt'
genes_to_process_list = []
if os.path.isfile(project_to_run_genes_filename):
	print 'Reading in to run genes from: ' + project_to_run_genes_filename
	file_in = open(project_to_run_genes_filename,'r')
	for line in file_in:
		line = line.strip()
		if not line.startswith("#"):
			genes_to_process_list.append(line)
	file_in.close()

# Read in accessions/samples to run ...
project_to_run_samples_filename = project_config['project_config_filepath_prefix']+'--to_run_samples.txt'
accessions_to_process_list = []
print "Deciding on where to get samples from ..."
if args.sample != '':
	print "Samples To Run: Using command link passed sample list"
	accessions_to_process_list.append(args.sample)
elif batch != '' and project_config.get('sample_mode','') == 'batch':
	if project_config.get('get_accession_using_starting_alignment_dir_postfix','') != '':	# we should get accessions by looking in alignment dir ...
		print "Samples To Run: Using get_accession_using_starting_alignment_dir_postfix: "+project_config['starting_alignment_dir']+'*'+project_config['get_accession_using_starting_alignment_dir_postfix']
		for file in glob.glob(project_config['starting_alignment_dir']+'*'+project_config['get_accession_using_starting_alignment_dir_postfix']):
			accessions_to_process_list.append(file.replace(project_config['starting_alignment_dir'], '').replace(project_config['raw_data_file_prefix'], '').replace(project_config['raw_data_file_postfix'], ''))
	else:
		print "Samples To Run: Using working_output_dir_batch_get_accession_using_folders: "+project_config['typing_results_dir_root']+'*'
		for file in glob.glob(project_config['typing_results_dir_root']+'*'):
			accessions_to_process_list.append(file.replace(project_config['typing_results_dir_root'], ''))
	accessions_to_process_list = sorted(accessions_to_process_list)
elif project_config.get('sample_mode','') == 'list':
	if os.path.isfile(project_to_run_samples_filename):
		print 'Reading in to run samples from: ' + project_to_run_samples_filename
		file_in = open(project_to_run_samples_filename,'r')
		for line in file_in:
			line = line.strip()
			if not line.startswith("#"):
				accessions_to_process_list.append(line)
		file_in.close()

# limit accessions_to_process_list based on batch start and size
if accession_batch_start and accession_batch_size:
	start_accession_index = int(accession_batch_start)-1
	end_accession_index = start_accession_index+int(accession_batch_size)
	accessions_to_process_list = accessions_to_process_list[start_accession_index:end_accession_index]
	print('start_accession_index: '+str(start_accession_index))
	print('end_accession_index: '+str(end_accession_index))
if just_summary:
	accessions_to_process_list = []
	summary = 1
print('accessions_to_process_list: ')+str(accessions_to_process_list)

# Go through each accession and run the pipeline ...
for accession in accessions_to_process_list:
	accession_cleaned = accession
	accession_search = re.search('(PM\d*-)\D?(\d*)', accession, re.IGNORECASE)
	if accession_search:
	    accession_cleaned = accession_search.group(1)+accession_search.group(2)
	print ('Working on: '+accession_cleaned)
	
	# create the working directory for the data files ...
	typing_results_dir_root = project_config['typing_results_dir_root']
	if not os.path.exists(typing_results_dir_root):
		print ('Creating working dir root: '+typing_results_dir_root)
		os.makedirs(typing_results_dir_root)
	typing_data_files_dir_root = project_config['typing_data_files_dir_root']
	if not os.path.exists(typing_data_files_dir_root):
		print ('Creating typing_data_files_dir_root: '+typing_data_files_dir_root)
		os.makedirs(typing_data_files_dir_root)
	working_output_dir = typing_data_files_dir_root+accession_cleaned+'/'
	if not os.path.exists(working_output_dir):
		print ('Creating working dir sample root: '+working_output_dir)
		os.makedirs(working_output_dir)
	working_output_dir = working_output_dir+'raw_data/'
	if not os.path.exists(working_output_dir):
		print ('Creating working dir: '+working_output_dir)
		os.makedirs(working_output_dir)
	full_gene_masters_dir = project_config['full_gene_master_alignments_dir']
	if not os.path.exists(full_gene_masters_dir):
		print ('Creating full_gene_masters_dir dir: '+full_gene_masters_dir)
		os.makedirs(full_gene_masters_dir)
		
	raw_data_subdir = ''
	if project_config['raw_data_file_in_accession_subdir'] == 'true':
		raw_data_subdir = accession+project_config.get('raw_data_file_in_accession_subdir_posfix','')+'/'
	#original_wgs_bam = project_config['starting_alignment_dir']+raw_data_subdir+project_config['raw_data_file_prefix']+accession+project_config['raw_data_file_postfix']
	master_full_gene_bam = full_gene_masters_dir+accession_cleaned+'.extract_for_bloodantigens-full_gene_master.bam'

	# see if we have a master full gene antigen bam and if not create if we can find original wgs data ...
	if project_config.get('sequence_type','') != 'SNP':
		print ('Looking for master_full_gene_bam: ' + master_full_gene_bam)
		if not os.path.isfile(master_full_gene_bam):
			print ('master_full_gene_bam not found')
			# we did not already have an extracted master_full_gene_bam, so search for original alignment and extract
			if os.path.isdir(project_config['starting_alignment_dir']+raw_data_subdir):
				print 'Found original BAM dir: '+project_config['starting_alignment_dir']+raw_data_subdir
				for searching_file in os.listdir(project_config['starting_alignment_dir']+raw_data_subdir):
					#print original_wgs_bam
					if fnmatch.fnmatch(searching_file, project_config['raw_data_file_prefix']+accession+project_config['raw_data_file_postfix']):
						original_wgs_bam = project_config['starting_alignment_dir']+raw_data_subdir+searching_file
						print ('Found original_wgs_bam: ' + original_wgs_bam)
						## 01_make_antigen_gene_master_bam.pl
						print "\nINFO: Step 1: Making Master All Gene blood-typing subset bam"
						bldt_cmd="""perl {typing_app_path}01_make_antigen_gene_master_bam.pl --tmp_dir='{tmp_dir}' --gatk_path='{gatk_path}' --ref_fasta_file='{ref_fasta_file}' --bam_file_in='{bam_file_in}' --extracted_bamfile_out='{extracted_bamfile_out}' --bldloci_list='{bldloci_list}'"""
						bldt_args={}
						bldt_args['typing_app_path']=app_config['app_client_code_dir']
						bldt_args['gatk_path']=project_config['gatk_filepath']
						bldt_args['ref_fasta_file']=project_config['ref_fasta_file']
						bldt_args['tmp_dir']='.'
						bldt_args['extracted_bamfile_out']=master_full_gene_bam
						bldt_args['bldloci_list']=project_config['BED_for_RBC_and_PLT_and_HNA_all_full_antigen_gene_master_filename']
						bldt_args['bam_file_in']=original_wgs_bam
						bldt_cmd=bldt_cmd.format(**bldt_args) 
						#run the script
						if '1' not in skip:
							print bldt_cmd,'\n'
							res=os.system(bldt_cmd)
							if res != 0: sys.exit(1)
						else:
							print "Skipping this step ..."
			else:
				print 'Could not find original BAM dir: '+project_config['starting_alignment_dir']+raw_data_subdir
		else:
			print "Found existing full gene master: "+master_full_gene_bam
	
	# make a mostly antigen exon only subset bam ...	
	if os.path.isfile(master_full_gene_bam) or project_config['sequence_type'] == 'SNP':
		## 02_make_exon_subset_bam.pl
		print "\nINFO: Step 2: Making mostly exon only blood-typing subset bam" 
		bldt_cmd="""perl {typing_app_path}02_make_exon_subset_bam.pl --tmp_dir='{tmp_dir}' --gatk_path='{gatk_path}' --ref_fasta_file='{ref_fasta_file}' --bam_file_in='{bam_file_in}' --extracted_bamfile_out='{extracted_bamfile_out}' --bldloci_list='{bldloci_list}'"""
		bldt_args={}
		bldt_args['typing_app_path']=app_config['app_client_code_dir']
		bldt_args['gatk_path']=project_config['gatk_filepath']
		bldt_args['ref_fasta_file']=project_config['ref_fasta_file']
		bldt_args['tmp_dir']='.'
		bldt_args['extracted_bamfile_out']=working_output_dir+accession_cleaned+'-extract_for_bloodantigens.bam'
		bldt_args['bldloci_list']=project_config['BED_for_just_in_use_antigen_specific_BAM_filename']
		bldt_args['bam_file_in']=master_full_gene_bam
		bldt_cmd=bldt_cmd.format(**bldt_args)
		#run the script
		if '2' not in skip:
			print bldt_cmd,'\n'
			res=os.system(bldt_cmd)
			if res != 0: sys.exit(1)
		else:
			print "Skipping this step..."	
			
		# 03_genotype_antigen_genes.pl
		print "\nINFO: Step 3: Running genotype blood-typing variants"
		bldt_cmd="""perl {typing_app_path}03_genotype_antigen_genes.pl --tmp_dir='{tmp_dir}' --gatk_path='{gatk_path}' --ref_fasta_file='{ref_fasta_file}' --bam_file_in='{bam_file_in}' --out_vcf='{out_vcf}' --bldloci_list='{bldloci_list}'"""
		bldt_args={}
		bldt_args['typing_app_path']=app_config['app_client_code_dir']
		bldt_args['gatk_path']=project_config['gatk_filepath']
		bldt_args['out_vcf']=working_output_dir+accession_cleaned+'_bld_type'+'.vcf'
		bldt_args['bldloci_list']=project_config['BED_for_exon_plus_filename']
		bldt_args['ref_fasta_file']=project_config['ref_fasta_file']
		bldt_args['tmp_dir']='.'
		bldt_args['bam_file_in']= master_full_gene_bam
		bldt_cmd=bldt_cmd.format(**bldt_args)
		#run the script
		if '3' not in skip:
			print bldt_cmd,'\n'
			res=os.system(bldt_cmd)
			if res != 0: sys.exit(1)
		else:
		    print "Skipping this step..."	    
		
		## 04_make_antigen_gene_coverage_file.pl
		print "\nINFO: Step 4: Getting coverage values"
		bldt_cmd="""perl {typing_app_path}04_make_antigen_gene_coverage_file.pl --ref_fasta_file='{ref_fasta_file}' --bed_file_in='{bed_file_in}' --bam_file_in='{bam_file_in}' --pileup_file='{pileup_file}' --raw_reads_file_out='{raw_reads_file_out}'"""
		bldt_args={}
		bldt_args['typing_app_path']=app_config['app_client_code_dir']
		bldt_args['ref_fasta_file']=project_config['ref_fasta_file']
		bldt_args['bed_file_in']=project_config['BED_for_just_in_use_antigen_specific_BAM_filename']
		bldt_args['bam_file_in']=working_output_dir+accession_cleaned+'-extract_for_bloodantigens.bam'
		bldt_args['pileup_file']=working_output_dir+accession_cleaned+'_genotype_raw_reads.mpileup'
		bldt_args['raw_reads_file_out']=working_output_dir+accession_cleaned+'_genotype_raw_reads.txt'
		bldt_cmd=bldt_cmd.format(**bldt_args)
		#run the script
		if '4' not in skip:
			print bldt_cmd,'\n'
			res=os.system(bldt_cmd)
			if res != 0: sys.exit(1)
		else:
		    print "Skipping this step..."
		    
		## 05_split_to_gene_vcf_seq_coverage_files.pl
		print "\nINFO: Step 5: Creating antigen gene specific vcf, coverage, and base call files" 
		bldt_cmd="""perl {typing_app_path}05_split_to_gene_vcf_seq_coverage_files.pl --accession_cleaned='{accession_cleaned}' --root_data_dir='{root_data_dir}' --root_results_dir='{root_results_dir}' --sequence_source='{sequence_source}' --instrument_type='{instrument_type}' --sequence_type='{sequence_type}' --antigen_systems='{antigen_systems}' --genes_to_predict='{genes_to_predict}' --probes_to_skip_filename='{probes_to_skip_filename}' --use_experimental_phasing_data='{use_experimental_phasing_data}'"""
		bldt_args={}
		bldt_args['typing_app_path']=app_config['app_client_code_dir']
		bldt_args['accession_cleaned']=accession_cleaned
		bldt_args['sequence_source']=project_config['sequence_source']
		bldt_args['instrument_type']=project_config['instrument_type']
		bldt_args['sequence_type']=project_config['sequence_type']
		bldt_args['root_data_dir']=project_config['typing_data_files_dir_root']
		bldt_args['root_results_dir']=project_config['typing_results_dir_root']
		bldt_args['antigen_systems']=project_config['antigen_systems']
		bldt_args['genes_to_predict']=",".join(genes_to_process_list)
		bldt_args['probes_to_skip_filename']=project_config['probes_to_skip_filename']
		bldt_args['use_experimental_phasing_data']=project_config.get('use_experimental_phasing_data', '')
		bldt_cmd=bldt_cmd.format(**bldt_args)
		#run the script
		if '5' not in skip:
			print bldt_cmd,'\n'
			res=os.system(bldt_cmd)
			if res != 0: sys.exit(1)
		else:
			print "Skipping this step..."	
			
		## 06_pre_typing_analysis.pl
		print "\nINFO: Step 6: Do some before typing analysis"
		bldt_cmd="""perl {typing_app_path}06_pre_typing_analysis.pl --accession_cleaned='{accession_cleaned}' --root_data_dir='{root_data_dir}' --root_results_dir='{root_results_dir}' --sequence_source='{sequence_source}' --instrument_type='{instrument_type}' --sequence_type='{sequence_type}' --probes_to_force_call_filename='{probes_to_force_call_filename}' --D_cnv_to_use_forced='{D_cnv_to_use_forced}' --C_cnv_to_use_forced='{C_cnv_to_use_forced}' --send_sample_name_to_typing_service='{send_sample_name_to_typing_service}' --website_version_override='{website_version_override}' --demo='{demo}' --access_code='{access_code}' --run_timestamp='{run_timestamp}'"""
		bldt_args={}
		bldt_args['typing_app_path']=app_config['app_client_code_dir']
		bldt_args['accession_cleaned']=accession_cleaned
		bldt_args['sequence_source']=project_config['sequence_source']
		bldt_args['instrument_type']=project_config['instrument_type']
		bldt_args['sequence_type']=project_config['sequence_type']
		bldt_args['root_data_dir']=project_config['typing_data_files_dir_root']
		bldt_args['root_results_dir']=project_config['typing_results_dir_root']
		bldt_args['antigen_systems']=project_config['antigen_systems']
		bldt_args['probes_to_force_call_filename']=project_config['probes_to_force_call_filename']
		bldt_args['D_cnv_to_use_forced']=project_config.get('D_cnv_to_use_forced','')
		bldt_args['C_cnv_to_use_forced']=project_config.get('C_cnv_to_use_forced','')
		bldt_args['send_sample_name_to_typing_service']=project_config.get('send_sample_name_to_typing_service', 'no')
		bldt_args['website_version_override']=project_config.get('website_version_override', '')
		bldt_args['demo']=demo
		bldt_args['access_code']=project_config.get('access_code', '')
		bldt_args['run_timestamp']=run_timestamp

		bldt_cmd=bldt_cmd.format(**bldt_args)
		#run the script
		if '6' not in skip:
			print bldt_cmd,'\n'
			res=os.system(bldt_cmd)
			if res != 0: sys.exit(1)
		else:
			print "Skipping this step..."
			
		## 07_type_antigens_from_alleles.pl.pl
		print "\nINFO: Step 7: Type antigens"
		bldt_cmd="""perl {typing_app_path}07_type_antigens_from_alleles.pl --accession_cleaned='{accession_cleaned}' --root_data_dir='{root_data_dir}' --root_results_dir='{root_results_dir}' --sequence_source='{sequence_source}' --instrument_type='{instrument_type}' --sequence_type='{sequence_type}' --antigen_systems='{antigen_systems}' --genes_to_predict='{genes_to_predict}' --nt_base_coverage_cutoff_for_call='{nt_base_coverage_cutoff_for_call}' --nt_base_coverage_cutoff_low_coverage_call='{nt_base_coverage_cutoff_low_coverage_call}' --no_coverage_plot='{no_coverage_plot}' --use_experimental_phasing_data='{use_experimental_phasing_data}' --include_misphase_in_results='{include_misphase_in_results}' --show_allele_network_graphs='{show_allele_network_graphs}' --show_coverage_table_regions_list='{show_coverage_table_regions_list}' --reference_GRCh_genome_build_num='{reference_GRCh_genome_build_num}' --send_sample_name_to_typing_service='{send_sample_name_to_typing_service}' --website_version_override='{website_version_override}' --demo='{demo}' --access_code='{access_code}' --run_timestamp='{run_timestamp}'"""
		bldt_args={}
		bldt_args['typing_app_path']=app_config['app_client_code_dir']
		bldt_args['accession_cleaned']=accession_cleaned
		bldt_args['sequence_source']=project_config['sequence_source']
		bldt_args['instrument_type']=project_config['instrument_type']
		bldt_args['sequence_type']=project_config['sequence_type']
		bldt_args['root_data_dir']=project_config['typing_data_files_dir_root']
		bldt_args['root_results_dir']=project_config['typing_results_dir_root']
		bldt_args['antigen_systems']=project_config['antigen_systems']
		bldt_args['genes_to_predict']=",".join(genes_to_process_list)
		bldt_args['no_coverage_plot']=project_config.get('no_coverage_plot', '')
		bldt_args['use_experimental_phasing_data']=project_config.get('use_experimental_phasing_data', '')
		bldt_args['include_misphase_in_results']=project_config.get('include_misphase_in_results', '')
		bldt_args['run_timestamp']=run_timestamp
		bldt_args['nt_base_coverage_cutoff_for_call']=project_config.get('nt_base_coverage_cutoff_for_call','')
		bldt_args['nt_base_coverage_cutoff_low_coverage_call']=project_config.get('nt_base_coverage_cutoff_low_coverage_call','')
		bldt_args['show_allele_network_graphs']=project_config.get('show_allele_network_graphs','')
		bldt_args['show_coverage_table_regions_list']=project_config.get('show_coverage_table_regions_list','')
		bldt_args['reference_GRCh_genome_build_num']=project_config.get('reference_GRCh_genome_build_num','')
		bldt_args['send_sample_name_to_typing_service']=project_config.get('send_sample_name_to_typing_service', 'no')
		bldt_args['website_version_override']=project_config.get('website_version_override', '')
		bldt_args['demo']=demo
		bldt_args['access_code']=project_config.get('access_code', '')

		bldt_cmd=bldt_cmd.format(**bldt_args) 
		#run the script
		if '7' not in skip:
			print bldt_cmd,'\n'
			res=os.system(bldt_cmd)
			if res != 0: sys.exit(1)
		else:
			print "Skipping this step..."
			
# run post pipeline script if there is one ...
if project_config.get('post_pipeline_script','') != '' and not just_summary:
	print "\nINFO: Step Post: Post pipeline script" 
	if 'post' not in skip:
		print 'Running post_pipeline_script: '+project_config['post_pipeline_script'],'\n'
		res=os.system(project_config['post_pipeline_script'])
		if res != 0: sys.exit(1)
	else:
		print "Skipping this step ..."

# run summary if asked ...
if summary:
	print "\nINFO: Step Summary: Creating Summary File"
	bldt_cmd="""python {typing_app_path}summary_create.py --typing_results_dir_root='{typing_results_dir_root}' --antigen_systems='{antigen_systems}' --dbsnp_file='{dbsnp_file}'"""
	bldt_args={}
	bldt_args['typing_app_path']=app_config['app_client_code_dir']
	bldt_args['typing_results_dir_root']=project_config['typing_results_dir_root']
	bldt_args['antigen_systems']=project_config['antigen_systems']
	bldt_args['dbsnp_file']=project_config.get('dbsnp_file', '')
	bldt_cmd=bldt_cmd.format(**bldt_args) 
	#run the script
	print bldt_cmd,'\n'
	res=os.system(bldt_cmd)
	if res != 0: sys.exit(1)